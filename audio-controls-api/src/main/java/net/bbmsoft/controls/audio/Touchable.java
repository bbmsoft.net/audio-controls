package net.bbmsoft.controls.audio;

import javafx.beans.property.BooleanProperty;
import javafx.scene.Node;

public interface Touchable<T extends Node> {

	public T getControl();
	
	public BooleanProperty touchedProperty();
	
	public default boolean isTouched() {
		return this.touchedProperty().get();
	}
}
