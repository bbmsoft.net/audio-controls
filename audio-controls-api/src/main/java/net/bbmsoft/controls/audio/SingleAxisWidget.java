package net.bbmsoft.controls.audio;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.geometry.Orientation;
import javafx.scene.control.Labeled;
import javafx.util.StringConverter;

public interface SingleAxisWidget extends AudioControl {

	public double getValue();

	public void setValue(double value);
	
	public DoubleProperty valueProperty();
	
	public double getRelativeValue();

	public double getMin();
	
	public void setMin(double value);
	
	public DoubleProperty minProperty();

	public double getMax();
	
	public void setMax(double value);
	
	public DoubleProperty maxProperty();
	
	public double getRange();
	
	public Curve getCurve();
	
	public ObjectProperty<Curve> curveProperty();
	
	public void setCurve(Curve value);
	
	public Orientation getOrientation();
	
	public ObjectProperty<Orientation> orientationProperty();

	public void setOrientation(Orientation value);
	
	public ObjectProperty<Scale> scaleProperty();
	
	public Scale getScale();
	
	public void setScale(Scale value);
	
	public void setLabel(Labeled label);
	
	public Labeled getLabel();
	
	public ObjectProperty<Labeled> labelProperty();

	public ObjectProperty<StringConverter<Double>> tickLabelConverterProperty();
	
	public StringConverter<Double> getTickLabelConverter();
	
	public void setTickLabelConverter(StringConverter<Double> value);

	public ObjectProperty<StringConverter<Double>> labelConverterProperty();

	public StringConverter<Double> getLabelConverter();

	public void setLabelConverter(StringConverter<Double> value);
}
