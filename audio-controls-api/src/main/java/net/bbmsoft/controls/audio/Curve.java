package net.bbmsoft.controls.audio;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;

public interface Curve {

	public double toInternalRatio(double value);
	
	public double toExternalValue(double ratio);
	
	public double toInternalDeltaRatio(double externalFrom, double externalTo);

	public default double changeExternalValueBy(double currentValue, double deltaRatio) {
		double ratio = this.toInternalRatio(currentValue);
		double newRatio = ratio + deltaRatio;
		return toExternalValue(newRatio);
	}

	public default double getMin() {
		return this.minProperty().get();
	}
	
	public default void setMin(double value) {
		this.minProperty().set(value);
	}
	
	public DoubleProperty minProperty();

	public default double getMax() {
		return this.maxProperty().get();
	}
	
	public default void setMax(double value) {
		this.maxProperty().set(value);
	}

	public DoubleProperty maxProperty();
	
	public boolean isInverted();
	
	public BooleanProperty invertedProperty();
	
	public default double getRange() {
		return this.getMax() - this.getMin();
	}
	
	public default double convert(double value, Curve other) {
		
		double ratio = this.toInternalRatio(value);
		double converted = other.toExternalValue(ratio);
		
		return converted;
	}
}
