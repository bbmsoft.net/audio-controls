package net.bbmsoft.controls.audio;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.paint.Paint;
import javafx.scene.text.TextAlignment;
import javafx.util.StringConverter;

public interface Scale extends AudioControl {

	public ObjectProperty<Curve> curveProperty();

	public Curve getCurve();

	public void setCurve(Curve value);

	public ObservableList<Double> getMajorTicks();

	public ObservableList<Double> getMinorTicks();

	public DoubleProperty majorTickLengthProperty();

	public double getMajorTickLength();

	public void setMajorTickLength(double value);

	public DoubleProperty minorTickLengthProperty();

	public double getMinorTickLength();

	public void setMinorTickLength(double value);

	public ObjectProperty<TextAlignment> alignmentProperty();

	public ObjectProperty<Orientation> orientationProperty();

	public ObjectProperty<HPos> hPosProperty();

	public ObjectProperty<VPos> vPosProperty();

	public Orientation getOrientation();

	public DoubleProperty topInsetProperty();

	public DoubleProperty rightInsetProperty();

	public DoubleProperty bottomInsetProperty();

	public DoubleProperty leftInsetProperty();

	public ObjectProperty<StringConverter<Double>> tickLabelConverterProperty();

	public StringConverter<Double> getTickLabelConverter();

	public void setTickLabelConverter(StringConverter<Double> value);

	public BooleanProperty showTickLabelsPropery();

	public boolean isShowTickLabels();

	public void setShowTickLabels(boolean value);

	public ObjectProperty<Paint> majorTickColorProperty();

	public Paint getMajorTickColor();

	public void setMajorTickColor(Paint value);

	public ObjectProperty<Paint> minorTickColorProperty();

	public Paint getMinorTickColor();

	public void setMinorTickColor(Paint value);

	public DoubleProperty minProperty();

	public double getMin();

	public void setMin(double value);

	public DoubleProperty maxProperty();

	public double getMax();

	public void setMax(double value);
	
	public ObjectProperty<Paint> tickLabelColorProperty();

	public Paint getTickLabelColor();

	public void setTickLabelColor(Paint value);
}
