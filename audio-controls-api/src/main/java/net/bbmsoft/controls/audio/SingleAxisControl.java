package net.bbmsoft.controls.audio;

import javafx.beans.property.ObjectProperty;
import javafx.scene.control.TextInputControl;

public interface SingleAxisControl extends SingleAxisWidget {
	
	public void setTextInput(TextInputControl textInput);
	
	public TextInputControl getTextInput();
	
	public ObjectProperty<TextInputControl> textInputProperty();
}
