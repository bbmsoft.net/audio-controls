package net.bbmsoft.controls.audio;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;

public interface Slider {

	public DoubleProperty verticalPixelRangeProperty();

	public double getVerticalPixelRange();

	public void setVerticalPixelRange(double value);

	public DoubleProperty horizontalPixelRangeProperty();

	public double getHorizontalPixelRange();

	public void setHorizontalPixelRange(double value);

	public DoubleProperty valueProperty();

	public double getValue();

	public void setValue(double value);

	public DoubleProperty minProperty();

	public double getMin();

	public void setMin(double value);

	public DoubleProperty maxProperty();

	public double getMax();

	public void setMax(double value);

	public DoubleProperty defaultValueProperty();

	public double getDefaultValue();

	public void setDefaultValue(double value);
	
	public ObjectProperty<Curve> curveProperty();
	
	public Curve getCurve();
	
	public void setCurve(Curve value);
	
	public BooleanProperty invertedProperty();
	
	public boolean isInverted();
	
	public void setInverted(boolean value);
	
	public ObjectProperty<TextField> textInputProperty();
	
	public TextField getTextInput();
	
	public void setTextInput(TextField value);
	
	public ObjectProperty<StringConverter<Double>> textInputConverterProperty();
	
	public StringConverter<Double> getTextInputConverter();
	
	public void setTextInputConverter(StringConverter<Double> value);
}
