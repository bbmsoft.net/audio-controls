package net.bbmsoft.controls.audio.example;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import net.bbmsoft.controls.audio.fader.TouchFader;
import net.bbmsoft.controls.audio.scale.TouchControlScale;
import net.bbmsoft.controls.audio.utils.LogarithmicCurve;

public class LogFaderTest extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		TouchControlScale scale = new TouchControlScale();
		TouchFader fader = new TouchFader();

		fader.setScale(scale);
		fader.setCurve(new LogarithmicCurve());
		fader.setMax(10_000);
		fader.setMin(10);

		scale.getMajorTicks().setAll(10.0, 100.0, 1_000.0, 10_000.0);
		scale.getMinorTicks().setAll(20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 200.0, 300.0, 400.0, 500.0, 600.0,
				700.0, 800.0, 900.0, 2000.0, 3000.0, 4000.0, 5000.0, 6000.0, 7000.0, 8000.0, 9000.0);
		scale.setMajorTickLength(50);
		scale.setMinorTickLength(45);
		scale.setAlignment(TextAlignment.CENTER);
		scale.setHPos(HPos.LEFT);

		primaryStage.setScene(new Scene(new StackPane(scale, fader)));
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
