package net.bbmsoft.controls.audio.example;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;

import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.StringConverter;
import net.bbmsoft.controls.audio.Fader;
import net.bbmsoft.controls.audio.Scale;
import net.bbmsoft.controls.audio.fader.TouchFader;
import net.bbmsoft.controls.audio.scale.TouchControlScale;
import net.bbmsoft.controls.audio.utils.BrokenCurve;
import net.bbmsoft.iocfx.StageService.ExitPolicy;
import net.bbmsoft.iocfx.Standalone;

@Component
public class FaderExampleApp extends HBox implements Standalone.Application, Initializable {

	private static int MAX = 15;
	private static int MIN_SCALE = -60;
	private static int MIN = -90;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		this.setSpacing(12);
		this.setPadding(new Insets(12));

		int count = 8;

		List<Double> majorTicks = new ArrayList<>();
		List<Double> minorTicks = new ArrayList<>();

		majorTicks.add((double) MIN);
		majorTicks.add((double) MAX);

		for (int i = MIN_SCALE; i < MAX; i++) {
			if (i % 10 == 0 || (i > -30 && i % 5 == 0)) {
				majorTicks.add((double) i);
			} else if (i % 5 == 0) {
				minorTicks.add((double) i);
			}
		}

		List<Fader> faders = new ArrayList<>();

		for (int i = 0; i < count; i++) {
			faders.add(addFader(majorTicks, minorTicks));
			this.getChildren().add(new Separator(Orientation.VERTICAL));
		}

		Fader masterFader = addFader(majorTicks, minorTicks);
		masterFader.valueProperty().addListener((o, ov, nv) -> faders.forEach(f -> f.setValue(nv.doubleValue())));

		// VBox vbox = new VBox();
		// for (int i = 0; i < count; i++) {
		// Fader service = this.faderFactory.getService();
		// service.setOrientation(Orientation.HORIZONTAL);
		// vbox.getChildren().add(service.getControl());
		// vbox.getChildren().add(new Separator(Orientation.HORIZONTAL));
		// }
		// this.getChildren().add(vbox);
	}

	private Fader addFader(List<Double> majorTicks, List<Double> minorTicks) {

		Fader fader = new TouchFader();
		Scale scale = new TouchControlScale();
		scale.getMajorTicks().addAll(majorTicks);
		scale.getMinorTicks().addAll(minorTicks);
		scale.setMajorTickLength(45.0);
		scale.setMinorTickLength(37.0);
		scale.hPosProperty().set(HPos.LEFT);
		scale.alignmentProperty().set(TextAlignment.CENTER);
		fader.setTickLabelConverter(new FaderLabelConverter());
		fader.setScale(scale);
//		fader.getControl().setPadding(new Insets(12, 4, 12, 4));
		fader.getControl().setPadding(new Insets(0, 48, 0, 24));
		fader.setMin(MIN);
		fader.setMax(MAX);
		Map<Double, Double> linearToBroken = new HashMap<>();
		// {0.7142857142857143=0.4999999999999999,
		// 0.8571428571428571=0.7533783783783783,
		// 0.5714285714285714=0.24999999999999994,
		// 0.2857142857142857=0.0506756756756757}
		linearToBroken.put(0.857, 0.75); // 0 dB
		linearToBroken.put(0.714, 0.5); // -15 dB
		linearToBroken.put(0.571, 0.25); // -30 dB
		linearToBroken.put(0.286, 0.05); // -60 dB
		fader.setCurve(new BrokenCurve(linearToBroken));
		StackPane stackPane = new StackPane(scale.getControl(), fader.getControl());
		TextField textField = new TextField();
		VBox vbox = new VBox(textField, stackPane);
		this.getChildren().add(vbox);
		fader.setTextInput(textField);
		VBox.setVgrow(stackPane, Priority.ALWAYS);

		return fader;
	}

	@Override
	public ExitPolicy getExitPolicy() {
		return ExitPolicy.DO_NOTHING_ON_STAGE_EXIT;
	}

	static class FaderLabelConverter extends StringConverter<Double> {
		
		private final DecimalFormat format = new DecimalFormat("0.#");

		@Override
		public String toString(Double tick) {

			if (tick <= MIN) {
				return "-∞";
			}

			return String.format("%s%s", tick < 0 ? "-" : (tick > 0 ? "+" : ""), this.format.format(Math.abs(tick)));
		}

		@Override
		public Double fromString(String string) {

			if (string.equals("-∞")) {
				return Double.valueOf(MIN);
			} else {
				return Double.parseDouble(string);
			}
		}
	}
}
