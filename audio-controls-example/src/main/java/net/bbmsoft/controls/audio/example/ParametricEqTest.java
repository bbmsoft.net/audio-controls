package net.bbmsoft.controls.audio.example;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import net.bbmsoft.controls.audio.eq.GraphicEQ;
import net.bbmsoft.controls.audio.eq.ParametricEQ;

public class ParametricEqTest extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		GraphicEQ graphicEQ = new GraphicEQ();
		ParametricEQ parametricEQ = new ParametricEQ();
		
		graphicEQ.setPrefSize(640, 320);
		parametricEQ.setPrefSize(640, 320);
		
		graphicEQ.setBandCount(5);
		parametricEQ.getBands().setAll(graphicEQ.getBands());
		
		primaryStage.setScene(new Scene(new VBox(graphicEQ, parametricEQ)));
		primaryStage.show();
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
