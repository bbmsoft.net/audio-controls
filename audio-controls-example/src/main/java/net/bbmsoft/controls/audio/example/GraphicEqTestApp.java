package net.bbmsoft.controls.audio.example;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.bbmsoft.controls.audio.eq.GraphicEQ;

public class GraphicEqTestApp extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		GraphicEQ eq = new GraphicEQ();
		eq.setBandCount(16);
		
		eq.getBands().get(0).setFrequency(20);
		eq.getBands().get(1).setFrequency(30);
		eq.getBands().get(2).setFrequency(50);
		eq.getBands().get(3).setFrequency(80);
		eq.getBands().get(4).setFrequency(120);
		eq.getBands().get(5).setFrequency(200);
		eq.getBands().get(6).setFrequency(300);
		eq.getBands().get(7).setFrequency(500);
		eq.getBands().get(8).setFrequency(800);
		eq.getBands().get(9).setFrequency(1200);
		eq.getBands().get(10).setFrequency(2000);
		eq.getBands().get(11).setFrequency(3000);
		eq.getBands().get(12).setFrequency(5000);
		eq.getBands().get(13).setFrequency(8000);
		eq.getBands().get(14).setFrequency(12000);
		eq.getBands().get(15).setFrequency(20000);
		
		primaryStage.setScene(new Scene(eq));
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

}
