package net.bbmsoft.controls.audio.utils;

import javafx.geometry.Bounds;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class TextUtils {

	private static final Text TEXT = new Text();
	
	public static Bounds getStringBounds(String string, Font font) {

		TEXT.setText(string);
		TEXT.setFont(font);

		final Bounds bounds = TEXT.getLayoutBounds();

		return bounds;

	}
}
