package net.bbmsoft.controls.audio.utils;

import javafx.beans.value.ObservableValue;

public class LinearCurve extends CurveBase {

	public LinearCurve() {
		super();
	}

	public LinearCurve(double min, double max, boolean inverted) {
		super(min, max, inverted);
	}

	public LinearCurve(ObservableValue<Number> min, ObservableValue<Number> max, ObservableValue<Boolean> inverted) {
		super(min, max, inverted);
	}

	@Override
	protected double toUnclampedUninvertedInternalRatio(double value) {
		return (value - this.getMin()) / this.getRange();
	}

	@Override
	public double toExternalValue(double ratio) {

		double clamp = this.clamp(0.0, ratio, 1.0);
		double inverted = this.invert(clamp);
		double value = this.getMin() + inverted * this.getRange();

		return value;
	}

}
