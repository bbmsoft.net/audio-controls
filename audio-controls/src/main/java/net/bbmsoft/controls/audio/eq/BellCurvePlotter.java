package net.bbmsoft.controls.audio.eq;

import static java.lang.Math.sqrt;

import net.bbmsoft.controls.audio.EQ;

public class BellCurvePlotter implements BandCurvePlotter {

	@Override
	public double computeGain(double frequency, EQ.Band band) {

		double f = band.getFrequency();
		double p = this.toPower(band.getGain());
		double pR = this.toPr(p);
		double q = band.getQ();

		double f0 = f / frequency;
		double f1 = square(f0);
		double f2 = square(1 - f1);
		double q2 = square(1 / q);

		double n = square(f2) + square(q2 * pR * f1) + (f2 * f1 * square(pR) * q2) + (f2 * f1 * q2);
		double d = square(f2 + q2 * f1);

		double pOut = p >= 1 ? sqrt(n / d) : sqrt(d / n);

		double gainOut = this.toDB(pOut);

		return gainOut;
	}
}
