package net.bbmsoft.controls.audio.scale;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.control.Skin;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.TextAlignment;
import javafx.util.StringConverter;
import net.bbmsoft.controls.audio.Curve;
import net.bbmsoft.controls.audio.Scale;
import net.bbmsoft.controls.audio.impl.TouchControlBase;
import net.bbmsoft.controls.audio.utils.LinearCurve;

public class TouchControlScale extends TouchControlBase implements Scale {

	private final ObservableList<Double> majorTicks;
	private final ObservableList<Double> minorTicks;
	private final ObjectProperty<Curve> curve;

	private final DoubleProperty majorTickLength;
	private final DoubleProperty minorTickLength;

	private final ObjectProperty<TextAlignment> alignment;
	private final ObjectProperty<Orientation> orientation;

	private final ObjectProperty<HPos> hPos;
	private final ObjectProperty<VPos> vPos;

	private final DoubleProperty topInset;
	private final DoubleProperty rightInset;
	private final DoubleProperty bottomInset;
	private final DoubleProperty leftInset;

	private final BooleanProperty showTickLabels;

	private final ObjectProperty<Paint> majorTickColor;
	private final ObjectProperty<Paint> minorTickColor;
	private final ObjectProperty<Paint> tickLabelColor;

	private final ObjectProperty<StringConverter<Double>> tickLabelConverter;

	public TouchControlScale() {

		this.majorTicks = FXCollections.observableArrayList();
		this.minorTicks = FXCollections.observableArrayList();
		this.curve = new SimpleObjectProperty<>(this, "curve", new LinearCurve());
		this.majorTickLength = new SimpleDoubleProperty(this, "major-tick-length", 8);
		this.minorTickLength = new SimpleDoubleProperty(this, "minor-tick-length", 6);
		this.alignment = new SimpleObjectProperty<>(this, "alignment", TextAlignment.LEFT);
		this.orientation = new SimpleObjectProperty<>(this, "orientation", Orientation.VERTICAL);

		this.hPos = new SimpleObjectProperty<>(this, "hPos", HPos.RIGHT);
		this.vPos = new SimpleObjectProperty<>(this, "vPos", VPos.BOTTOM);

		this.topInset = new SimpleDoubleProperty(this, "top-inset", 0.0);
		this.rightInset = new SimpleDoubleProperty(this, "rigth-inset", 0.0);
		this.bottomInset = new SimpleDoubleProperty(this, "bottom-inset", 0.0);
		this.leftInset = new SimpleDoubleProperty(this, "left-inset", 0.0);

		this.majorTickColor = new SimpleObjectProperty<>(this, "major-tick-color", Color.BLACK);
		this.minorTickColor = new SimpleObjectProperty<>(this, "minor-tick-color", Color.BLACK);
		this.tickLabelColor = new SimpleObjectProperty<>(this, "tick-label-color", Color.BLACK);

		this.tickLabelConverter = new SimpleObjectProperty<StringConverter<Double>>(this, "tick-label-converter", null);

		this.showTickLabels = new SimpleBooleanProperty(this, "show-ticklabels", true);

		this.setFocusTraversable(false);
		this.setMouseTransparent(true);
	}

	@Override
	protected Skin<TouchControlScale> createDefaultSkin() {
		return new ScaleSkin(this);
	}

	@Override
	public ObjectProperty<Curve> curveProperty() {
		return this.curve;
	}

	@Override
	public ObservableList<Double> getMajorTicks() {
		return this.majorTicks;
	}

	@Override
	public ObservableList<Double> getMinorTicks() {
		return this.minorTicks;
	}

	@Override
	public DoubleProperty majorTickLengthProperty() {
		return this.majorTickLength;
	}

	@Override
	public double getMajorTickLength() {
		return this.majorTickLength.get();
	}

	@Override
	public void setMajorTickLength(double value) {
		this.majorTickLength.set(value);
	}

	@Override
	public DoubleProperty minorTickLengthProperty() {
		return this.minorTickLength;
	}

	public double getMinorTickLength() {
		return this.minorTickLength.get();
	}

	public void setMinorTickLength(double value) {
		this.minorTickLength.set(value);
	}

	@Override
	public ObjectProperty<TextAlignment> alignmentProperty() {
		return this.alignment;
	}

	public TextAlignment getAlignment() {
		return this.alignment.get();
	}

	public void setAlignment(TextAlignment value) {
		this.alignment.set(value);
	}

	@Override
	public ObjectProperty<Orientation> orientationProperty() {
		return this.orientation;
	}

	public Orientation getOrientation() {
		return this.orientation.get();
	}

	public void setOrientation(Orientation value) {
		this.orientation.set(value);
	}

	@Override
	public DoubleProperty topInsetProperty() {
		return this.topInset;
	}

	@Override
	public DoubleProperty rightInsetProperty() {
		return this.rightInset;
	}

	@Override
	public DoubleProperty bottomInsetProperty() {
		return this.bottomInset;
	}

	@Override
	public DoubleProperty leftInsetProperty() {
		return this.leftInset;
	}

	@Override
	public ObjectProperty<StringConverter<Double>> tickLabelConverterProperty() {
		return this.tickLabelConverter;
	}

	@Override
	public ObjectProperty<HPos> hPosProperty() {
		return this.hPos;
	}

	public void setHPos(HPos value) {
		this.hPos.set(value);
	}

	public HPos getHPos() {
		return this.hPos.get();
	}

	@Override
	public ObjectProperty<VPos> vPosProperty() {
		return this.vPos;
	}

	public void setVPos(VPos value) {
		this.vPos.set(value);
	}

	public VPos getVPos() {
		return this.vPos.get();
	}

	@Override
	public BooleanProperty showTickLabelsPropery() {
		return this.showTickLabels;
	}

	@Override
	public boolean isShowTickLabels() {
		return this.showTickLabels.get();
	}

	@Override
	public void setShowTickLabels(boolean value) {
		this.showTickLabels.set(value);
	}

	@Override
	public ObjectProperty<Paint> majorTickColorProperty() {
		return this.majorTickColor;
	}

	@Override
	public Paint getMajorTickColor() {
		return this.majorTickColor.get();
	}

	@Override
	public void setMajorTickColor(Paint value) {
		this.majorTickColor.set(value);
	}

	@Override
	public ObjectProperty<Paint> minorTickColorProperty() {
		return this.minorTickColor;
	}

	@Override
	public Paint getMinorTickColor() {
		return this.minorTickColor.get();
	}

	@Override
	public void setMinorTickColor(Paint value) {
		this.minorTickColor.set(value);
	}

	@Override
	public DoubleProperty minProperty() {
		return this.getCurve().minProperty();
	}

	@Override
	public double getMin() {
		return minProperty().get();
	}

	@Override
	public void setMin(double value) {
		this.minProperty().set(value);
	}

	@Override
	public DoubleProperty maxProperty() {
		return this.getCurve().maxProperty();
	}

	@Override
	public double getMax() {
		return maxProperty().get();
	}

	@Override
	public void setMax(double value) {
		this.maxProperty().set(value);
	}

	@Override
	public Curve getCurve() {
		return this.curve.get();
	}

	@Override
	public void setCurve(Curve value) {
		this.curve.set(value);
	}

	@Override
	public StringConverter<Double> getTickLabelConverter() {
		return this.tickLabelConverter.get();
	}

	@Override
	public void setTickLabelConverter(StringConverter<Double> value) {
		this.tickLabelConverter.set(value);
	}
	
	@Override
	public ObjectProperty<Paint> tickLabelColorProperty() {
		return this.tickLabelColor;
	}
	
	@Override
	public Paint getTickLabelColor() {
		return this.tickLabelColor.get();
	}
	
	@Override
	public void setTickLabelColor(Paint value) {
		this.tickLabelColor.set(value);
	}
	
	public double getTopInset() {
		return this.topInset.get();
	}
	
	public void setTopInset(double value) {
		this.topInset.set(value);
	}
	
	public double getBottomInset() {
		return this.bottomInset.get();
	}
	
	public void setBottomInset(double value) {
		this.bottomInset.set(value);
	}
	
	public double getLeftInset() {
		return this.topInset.get();
	}
	
	public void setLeftInset(double value) {
		this.topInset.set(value);
	}
	
	public double getRightInset() {
		return this.rightInset.get();
	}
	
	public void setRightInset(double value) {
		this.rightInset.set(value);
	}
	
}
