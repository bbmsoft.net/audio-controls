package net.bbmsoft.controls.audio.eq;

import java.text.DecimalFormat;

import javafx.util.StringConverter;

public class FrequencyTextFieldConverter extends StringConverter<Double> {

	private final DecimalFormat format;
	private final DecimalFormat format1k;
	private final DecimalFormat format10k;
	
	public FrequencyTextFieldConverter() {
		this(true);
	}
	
	public FrequencyTextFieldConverter(boolean showUnit) {
		this.format = new DecimalFormat(showUnit ? "0 Hz" : "0");
		this.format1k = new DecimalFormat(showUnit ? "0.00 kHz" : "0.00");
		this.format10k = new DecimalFormat(showUnit ? "0.0 kHz" : "0.0");
	}

	@Override
	public String toString(Double object) {

		double doubleValue = object.doubleValue();

		StringBuilder sb = new StringBuilder();

		if (doubleValue < 1_000) {
			sb.append(this.format.format(doubleValue));
		} else if (doubleValue < 10_000) {
			sb.append(this.format1k.format(doubleValue / 1_000));
		} else {
			sb.append(this.format10k.format(doubleValue / 1_000));
		}

		return sb.toString();
	}

	@Override
	public Double fromString(String string) {

		String truncated = string;
		boolean thousand = false;

		if (string.endsWith(" Hz")) {
			truncated = string.substring(0, string.length() - 3);
		} else if (string.endsWith("Hz")) {
			truncated = string.substring(0, string.length() - 2);
		}

		if (truncated.endsWith(" k")) {
			thousand = true;
			truncated = truncated.substring(0, truncated.length() - 2);
		} else if (truncated.endsWith("k")) {
			thousand = true;
			truncated = truncated.substring(0, truncated.length() - 1);
		}

		double value = Double.parseDouble(truncated.trim());

		return thousand ? value * 1_000 : value;
	}

}
