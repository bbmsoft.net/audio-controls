package net.bbmsoft.controls.audio.scale;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.SkinBase;
import javafx.scene.paint.Paint;
import javafx.scene.text.TextAlignment;
import javafx.util.StringConverter;
import net.bbmsoft.controls.audio.Curve;
import net.bbmsoft.controls.audio.impl.ResizableCanvas;
import net.bbmsoft.controls.audio.utils.LinearCurve;
import net.bbmsoft.controls.audio.utils.TextUtils;

public class ScaleSkin extends SkinBase<TouchControlScale> {

	private static final int TICK_LABEL_GAP = 2;

	private final ResizableCanvas canvas;

	private InvalidationListener insetsListener;

	public ScaleSkin(TouchControlScale control) {

		super(control);

		this.canvas = new ResizableCanvas();
		this.getChildren().add(this.canvas);

		this.insetsListener = o -> this.getSkinnable().requestLayout();

		this.getSkinnable().insetsProperty().addListener(this.insetsListener);

	}

	@Override
	protected void layoutInArea(Node child, double areaX, double areaY, double areaWidth, double areaHeight,
			double areaBaselineOffset, Insets margin, boolean fillWidth, boolean fillHeight, HPos halignment,
			VPos valignment) {

		super.layoutInArea(child, areaX, areaY, areaWidth, areaHeight, areaBaselineOffset, margin, fillWidth,
				fillHeight, halignment, valignment);
		
		if(child != this.canvas) {
			return;
		}

		Platform.runLater(() -> redrawScale());

	}

	private void redrawScale() {
		
		double areaWidth = this.canvas.getWidth();
		double areaHeight = this.canvas.getHeight();
		
		GraphicsContext g = this.canvas.getGraphicsContext2D();
		g.clearRect(0, 0, this.canvas.getWidth(), this.canvas.getHeight());

		TouchControlScale scale = this.getSkinnable();

		Curve curve = scale.getCurve();

		if (curve == null) {
			return;
		}

		for (Double tick : scale.getMajorTicks()) {
			double pos = curve.convert(tick, this.canvasCurve());
			this.drawTick(tick, pos, scale.getMajorTickLength(), scale.getMajorTickColor(), scale.isShowTickLabels(),
					areaWidth, areaHeight);
		}

		for (Double tick : scale.getMinorTicks()) {
			double pos = curve.convert(tick, this.canvasCurve());
			this.drawTick(tick, pos, scale.getMinorTickLength(), scale.getMinorTickColor(), false, areaWidth,
					areaHeight);
		}
	}

	private Curve canvasCurve() {

		TouchControlScale scale = this.getSkinnable();

		double min = this.isVertical() ? scale.topInsetProperty().get() : scale.leftInsetProperty().get();
		double max = this.isVertical() ? this.canvas.getHeight() - scale.bottomInsetProperty().get()
				: this.canvas.getWidth() - scale.rightInsetProperty().get();

		return new LinearCurve(min + 0.5, max - 0.5, this.isVertical());
	}

	private boolean isVertical() {
		return this.getSkinnable().getOrientation() == Orientation.VERTICAL;
	}

	private void drawTick(Double tick, double pos, double lenght, Paint color, boolean drawTickLabel, double areaWidth,
			double areaHeight) {

		TouchControlScale scale = this.getSkinnable();

		GraphicsContext g = this.canvas.getGraphicsContext2D();

		g.setStroke(color);
		g.setFill(scale.getTickLabelColor());

		Orientation orientation = scale.orientationProperty().get();
		TextAlignment alignment = scale.alignmentProperty().get();

		String tickLabel = null;

		if (drawTickLabel) {
			StringConverter<Double> tickLabelConverter = scale.getTickLabelConverter();
			tickLabel = tickLabelConverter != null ? tickLabelConverter.toString(tick) : String.valueOf(tick);
		}

		if (orientation == Orientation.VERTICAL) {

			HPos hpos = scale.hPosProperty().get();

			switch (hpos) {
			case CENTER:
				this.drawVerticalCenterTick(tickLabel, pos, lenght, alignment, areaWidth);
				break;
			case LEFT:
				this.drawVerticalLeftTick(tickLabel, pos, lenght, alignment, areaWidth);
				break;
			case RIGHT:
				this.drawVerticalRightTick(tickLabel, pos, lenght, alignment, areaWidth);
				break;
			default:
				throw new IllegalStateException("Unknown position: " + pos);

			}

		} else {

			VPos vpos = scale.vPosProperty().get();

			switch (vpos) {
			case CENTER:
				this.drawHorizontalCenterTick(tickLabel, pos, lenght, alignment, areaHeight);
				break;
			case TOP:
				this.drawHorizontalTopTick(tickLabel, pos, lenght, alignment, areaHeight);
				break;
			case BOTTOM:
				this.drawHorizontalBottomTick(tickLabel, pos, lenght, alignment, areaHeight);
				break;
			default:
				throw new IllegalStateException("Unknown position: " + pos);

			}
		}

	}

	private void drawVerticalCenterTick(String tickLabel, double pos, double lenght, TextAlignment alignment,
			double areaWidth) {
		// TODO Auto-generated method stub

	}

	private void drawVerticalLeftTick(String tickLabel, double pos, double lenght, TextAlignment alignment,
			double areaWidth) {

		TouchControlScale scale = this.getSkinnable();

		GraphicsContext g = this.canvas.getGraphicsContext2D();

		Bounds labelBounds = tickLabel != null ? TextUtils.getStringBounds(tickLabel, g.getFont())
				: new BoundingBox(0, 0, 0, 0);
		double labelWidth = labelBounds.getWidth();
		double labelHeight = labelBounds.getHeight();
		double widthReservedForLabel = labelWidth + (tickLabel != null ? TICK_LABEL_GAP : 0.0);

		double overallLength = widthReservedForLabel + lenght;

		double tickX, labelX, actualTickLength;

		switch (alignment) {
		case CENTER:
			actualTickLength = lenght;
			tickX = (areaWidth - actualTickLength + scale.leftInsetProperty().get() - scale.rightInsetProperty().get())
					/ 2;
			labelX = tickX - widthReservedForLabel;
			break;
		case JUSTIFY:
			labelX = TICK_LABEL_GAP;
			tickX = labelX + widthReservedForLabel;
			actualTickLength = areaWidth - widthReservedForLabel;
			break;
		case LEFT:
			labelX = scale.leftInsetProperty().get();
			tickX = labelX + widthReservedForLabel;
			actualTickLength = lenght;
			break;
		case RIGHT:
			labelX = areaWidth - overallLength;
			tickX = labelX + widthReservedForLabel;
			actualTickLength = lenght;
			break;
		default:
			throw new IllegalStateException("Unknown alignment: " + alignment);
		}

		double snappedPos = Math.round(pos) + 0.5;
		double snappedLabelX = Math.round(labelX);
		double snappedTickX = Math.round(tickX);
		double snappedLength = Math.ceil(actualTickLength) - 0.5;

		g.strokeLine(snappedTickX, snappedPos, snappedTickX + snappedLength, snappedPos);

		if (tickLabel != null) {
			g.fillText(tickLabel, snappedLabelX, Math.round(pos + labelHeight * 0.33));
		}

	}

	private void drawHorizontalBottomTick(String tickLabel, double pos, double lenght, TextAlignment alignment,
			double areaHeight) {

		GraphicsContext g = this.canvas.getGraphicsContext2D();

		Bounds labelBounds = tickLabel != null ? TextUtils.getStringBounds(tickLabel, g.getFont())
				: new BoundingBox(0, 0, 0, 0);
		double labelWidth = labelBounds.getWidth();
		double labelHeight = labelBounds.getHeight() * 0.67;
		double heightReservedForLabel = labelHeight + (tickLabel != null ? TICK_LABEL_GAP : 0.0);

		double overallLength = heightReservedForLabel + lenght;

		double tickY, labelY, actualTickLength;

		switch (alignment) {
		case CENTER:
			// TODO
			actualTickLength = lenght;
			tickY = (areaHeight - actualTickLength) / 2;
			labelY = tickY - heightReservedForLabel;
			break;
		case JUSTIFY:
			// TODO
			labelY = TICK_LABEL_GAP + labelHeight;
			tickY = TICK_LABEL_GAP + heightReservedForLabel;
			actualTickLength = areaHeight - heightReservedForLabel;
			break;
		case LEFT:
			actualTickLength = lenght;
			tickY = areaHeight - actualTickLength;
			labelY = tickY - TICK_LABEL_GAP;
			break;
		case RIGHT:
			// TODO
			labelY = areaHeight - overallLength;
			tickY = 0.0;
			actualTickLength = lenght;
			break;
		default:
			throw new IllegalStateException("Unknown alignment: " + alignment);
		}

		double snappedPos = Math.round(pos) + 0.5;
		double snappedLabelY = Math.round(labelY);
		double snappedTickY = Math.round(tickY);
		double snappedLength = Math.ceil(actualTickLength) - 0.5;

		g.strokeLine(snappedPos, snappedTickY, snappedPos, snappedTickY + snappedLength);

		if (tickLabel != null) {
			g.fillText(tickLabel, Math.round(pos - labelWidth / 2), snappedLabelY);
		}

	}

	private void drawHorizontalCenterTick(String tickLabel, double pos, double lenght, TextAlignment alignment,
			double areaHeight) {
		// TODO Auto-generated method stub

	}

	private void drawHorizontalTopTick(String tickLabel, double pos, double lenght, TextAlignment alignment,
			double areaHeight) {

		GraphicsContext g = this.canvas.getGraphicsContext2D();

		Bounds labelBounds = tickLabel != null ? TextUtils.getStringBounds(tickLabel, g.getFont())
				: new BoundingBox(0, 0, 0, 0);
		double labelWidth = labelBounds.getWidth();
		double labelHeight = labelBounds.getHeight() * 0.67;
		double heightReservedForLabel = labelHeight + (tickLabel != null ? TICK_LABEL_GAP : 0.0);

		double overallLength = heightReservedForLabel + lenght;

		double tickY, labelY, actualTickLength;

		switch (alignment) {
		case CENTER:
			actualTickLength = lenght;
			tickY = (areaHeight - actualTickLength) / 2;
			labelY = tickY - heightReservedForLabel;
			break;
		case JUSTIFY:
			labelY = TICK_LABEL_GAP + labelHeight;
			tickY = TICK_LABEL_GAP + heightReservedForLabel;
			actualTickLength = areaHeight - heightReservedForLabel;
			break;
		case LEFT:
			labelY = labelHeight;
			tickY = heightReservedForLabel;
			actualTickLength = lenght;
			break;
		case RIGHT:
			labelY = areaHeight - overallLength;
			tickY = 0.0;
			actualTickLength = lenght;
			break;
		default:
			throw new IllegalStateException("Unknown alignment: " + alignment);
		}

		double snappedPos = Math.round(pos) + 0.5;
		double snappedLabelY = Math.round(labelY);
		double snappedTickY = Math.round(tickY);
		double snappedLength = Math.ceil(actualTickLength) - 0.5;

		g.strokeLine(snappedPos, snappedTickY, snappedPos, snappedTickY + snappedLength);

		if (tickLabel != null) {
			g.fillText(tickLabel, Math.round(pos - labelWidth / 2), snappedLabelY);
		}

	}

	private void drawVerticalRightTick(String tickLabel, double pos, double lenght, TextAlignment alignment,
			double areaWidth) {

		TouchControlScale scale = this.getSkinnable();

		GraphicsContext g = this.canvas.getGraphicsContext2D();

		Bounds labelBounds = tickLabel != null ? TextUtils.getStringBounds(tickLabel, g.getFont())
				: new BoundingBox(0, 0, 0, 0);
		double labelWidth = labelBounds.getWidth();
		double labelHeight = labelBounds.getHeight();
		double widthReservedForLabel = labelWidth + (tickLabel != null ? TICK_LABEL_GAP : 0.0);

		double overallLength = widthReservedForLabel + lenght;

		double tickX, labelX, actualTickLength;

		switch (alignment) {
		case CENTER:
			actualTickLength = lenght;
			tickX = (areaWidth - actualTickLength + scale.leftInsetProperty().get() - scale.rightInsetProperty().get())
					/ 2;
			labelX = tickX - widthReservedForLabel;
			break;
		case JUSTIFY:
			labelX = TICK_LABEL_GAP;
			tickX = labelX + widthReservedForLabel;
			actualTickLength = areaWidth - widthReservedForLabel;
			break;
		case LEFT:
			tickX = scale.leftInsetProperty().get();
			labelX = tickX + lenght + TICK_LABEL_GAP;
			actualTickLength = lenght;
			break;
		case RIGHT:
			labelX = areaWidth - overallLength;
			tickX = labelX + widthReservedForLabel;
			actualTickLength = lenght;
			break;
		default:
			throw new IllegalStateException("Unknown alignment: " + alignment);
		}

		double snappedPos = Math.round(pos) + 0.5;
		double snappedLabelX = Math.round(labelX);
		double snappedTickX = Math.round(tickX);
		double snappedLength = Math.ceil(actualTickLength) - 0.5;

		g.strokeLine(snappedTickX, snappedPos, snappedTickX + snappedLength, snappedPos);

		if (tickLabel != null) {
			g.fillText(tickLabel, snappedLabelX, Math.round(pos + labelHeight * 0.33));
		}

	}

	@Override
	public void dispose() {
		this.getChildren().remove(this.canvas);
		this.getSkinnable().insetsProperty().removeListener(this.insetsListener);
		super.dispose();
	}

}
