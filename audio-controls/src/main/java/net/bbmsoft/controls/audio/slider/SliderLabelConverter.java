package net.bbmsoft.controls.audio.slider;

import java.text.DecimalFormat;

import javafx.util.StringConverter;

public class SliderLabelConverter extends StringConverter<Double> {

	private final DecimalFormat format;
	
	public SliderLabelConverter() {
		this("0.#");
	}
	
	public SliderLabelConverter(String format) {
		this.format = new DecimalFormat(format);
	}	

	@Override
	public String toString(Double tick) {
		return String.format("%s%s", tick < 0 ? "-" : (tick > 0 ? "+" : ""), this.format.format(Math.abs(tick)));
	}

	@Override
	public Double fromString(String string) {
		return Double.parseDouble(string);
	}
}
