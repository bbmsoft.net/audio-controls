package net.bbmsoft.controls.audio.eq;

public class CurveCoeffs {

	static final double[][] coeffs = {
			{ 1d, 0d, 0d, 0d, 0d, 0d },
			{ 1.4142d, 0d, 0d, 1d, 0d, 0d },
			{ 1d, 1d, 0d, 0d, 1d, 0d },
			{ 1.8478d, 0.7654d, 0d, 1d, 1d, 0d },
			{ 1d, 1.6180d, 0.6180d, 0d, 1d, 1d },
			{ 1.3617d, 1.3617d, 0d, 0.6180d, 0.6180d, 0d },
			{ 1.4142d, 1.4142d, 0d, 1d, 1d, 0d }
		};
}
