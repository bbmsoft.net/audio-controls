package net.bbmsoft.controls.audio.impl;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextInputControl;
import javafx.util.StringConverter;
import net.bbmsoft.controls.audio.Fader;

public abstract class SingleAxisControlBase extends SingleAxisWidetBase implements Fader {

	private final ObjectProperty<TextInputControl> textInput;
	private final EventHandler<ActionEvent> textListener;
	private final StringConverter<Double> textFieldConverter;

	public SingleAxisControlBase(StringConverter<Double> textFieldConverter) {

		this.textInput = new SimpleObjectProperty<TextInputControl>(this, "text-input", null);
		this.textListener = this::textInputUpdated;
		this.textInput.addListener((o, ov, nv) -> {
			if (ov != null) {
				this.disconnect(ov);
			}
			if (nv != null) {
				this.connect(nv);
			}
		});

		this.textFieldConverter = textFieldConverter;

		this.valueProperty().addListener((o, ov, nv) -> this.updateTextInput(nv.doubleValue()));
	}

	@Override
	public void setTextInput(TextInputControl textInput) {
		this.textInput.set(textInput);
	}

	@Override
	public ObjectProperty<TextInputControl> textInputProperty() {
		return this.textInput;
	}

	@Override
	public TextInputControl getTextInput() {
		return this.textInput.get();
	}

	private void connect(TextInputControl textInput) {

		textInput.addEventHandler(ActionEvent.ACTION, this.textListener);
		this.updateTextInput(this.getValue());
	}

	private void disconnect(TextInputControl textInput) {
		textInput.removeEventHandler(ActionEvent.ACTION, this.textListener);
	}

	private void updateTextInput(Double newValue) {

		TextInputControl textInputControl = this.textInput.get();

		if (textInputControl == null) {
			return;
		}

		textInputControl.setText(this.textFieldConverter.toString(newValue));
	}

	private void textInputUpdated(ActionEvent e) {

		if (this.isTouched()) {
			return;
		}

		String text = ((TextInputControl) e.getSource()).getText();

		try {

			double newValue = this.textFieldConverter.fromString(text);
			this.setValue(newValue);

		} catch (NumberFormatException ex) {
			Platform.runLater(() -> this.updateTextInput(this.getValue()));
		}
	}

	public StringConverter<Double> getTextFieldConverter() {
		return textFieldConverter;
	}

}
