package net.bbmsoft.controls.audio.eq;

import static java.lang.Math.sqrt;

import net.bbmsoft.controls.audio.EQ.Band;

public class HighShelfCurvePlotter implements BandCurvePlotter {

	@Override
	public double computeGain(double frequency, Band band) {

		double f = band.getFrequency();
		double p = this.toPower(band.getGain());
		double pR = this.toPr(p);

		double f0 = f / frequency;
		double f1 = square(f0);
		double f2 = square(1 - f1);
		double f3;

		double d = square(f2 + 2 * f1);
		double n;
		double pOut;

		if (p >= 1) {
			f3 = square(p - f1);
			n = (f3 * f2) + (4 * p * f1 * f1) + (2 * p * f1 * f2) + (2 * f1 * f3);
			pOut = sqrt(n / d);
		} else {
			f3 = square(pR - f1);
			n = (f2 * f3) + (4 * pR * f1 * f1) + (2 * f1 * f3) + (2 * pR * f1 * f2);
			pOut = sqrt(d / n);
		}

		double gainOut = this.toDB(pOut);

		return gainOut;
	}
}
