package net.bbmsoft.controls.audio.slider;

import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.SkinBase;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import net.bbmsoft.controls.audio.Curve;
import net.bbmsoft.controls.audio.impl.MultiTouchHelper;
import net.bbmsoft.controls.audio.impl.MultiTouchHelper.Update;
import net.bbmsoft.controls.audio.utils.LinearCurve;

public class PotiSliderSkin extends SkinBase<SliderBase> {

	private final Circle circle;
	private final Rectangle indicator;

	private final EventHandler<KeyEvent> keyPressListener;
	private final EventHandler<KeyEvent> keyReleaseListener;
	private final EventHandler<ScrollEvent> scrollListener;
	private final EventHandler<MouseEvent> clickListener;

	private MultiTouchHelper mth;

	private boolean shiftDown;

	private int orientationBias;

	public PotiSliderSkin(SliderBase slider) {

		super(slider);

		this.circle = new Circle();
		this.indicator = new Rectangle();

		this.keyPressListener = this::keyPressed;
		this.keyReleaseListener = this::keyReleased;
		this.scrollListener = this::scrolled;
		this.clickListener = this::clicked;

		this.circle.getStyleClass().add("poti");
		this.indicator.getStyleClass().add("indicator");

		this.getChildren().addAll(this.circle, this.indicator);

		this.addListeners();
	}

	@Override
	protected void layoutInArea(Node child, double areaX, double areaY, double areaWidth, double areaHeight,
			double areaBaselineOffset, Insets margin, boolean fillWidth, boolean fillHeight, HPos halignment,
			VPos valignment) {

		double diameter = Math.min(areaWidth, areaHeight);

		double radius = diameter / 2;
		this.circle.setRadius(radius);

		double x, y;

		switch (halignment) {
		case CENTER:
			x = areaX + (areaWidth - diameter) / 2;
			break;
		case LEFT:
			x = areaX;
			break;
		case RIGHT:
			x = areaX + areaWidth - diameter;
			break;
		default:
			throw new IllegalStateException("Unknown hpos: " + halignment);
		}

		switch (valignment) {
		case BASELINE:
		case BOTTOM:
			y = areaY + areaHeight - diameter;
			break;
		case CENTER:
			y = areaY + (areaHeight - diameter) / 2;
			break;
		case TOP:
			y = areaY;
			break;
		default:
			throw new IllegalStateException("Unknown vpos: " + valignment);
		}

		this.circle.relocate(x, y);

		double height = diameter / 3.5;
		double width = diameter / 16;

		this.indicator.setHeight(height);
		this.indicator.setWidth(width);

		this.indicator.relocate(areaX + (areaWidth - width) / 2, areaY + y);

		SliderBase slider = this.getSkinnable();

		double minAngle = -140;
		double maxAngle = 140;
		
		Curve angleCurve = new LinearCurve(minAngle, maxAngle, false);
		double angle = slider.getCurve().convert(slider.getValue(), angleCurve);

		Rotate rotate = new Rotate(angle, width / 2, radius);
		this.indicator.getTransforms().setAll(rotate);
	}

	@Override
	public void dispose() {
		this.removeListeners();
		super.dispose();
	}

	private void addListeners() {

		SliderBase slider = this.getSkinnable();

		this.mth = new MultiTouchHelper(slider);
		this.mth.setOnTouchPressed(this::pressed);
		this.mth.setOnTouchMoved(this::dragged);
		this.mth.setOnTouchReleased(this::released);

		slider.addEventHandler(KeyEvent.KEY_PRESSED, this.keyPressListener);
		slider.addEventHandler(KeyEvent.KEY_RELEASED, this.keyReleaseListener);
		slider.addEventHandler(ScrollEvent.SCROLL, this.scrollListener);
		slider.addEventHandler(MouseEvent.MOUSE_CLICKED, this.clickListener);
	}

	private void removeListeners() {

		this.mth.dispose();

		SliderBase slider = this.getSkinnable();

		slider.removeEventHandler(MouseEvent.MOUSE_CLICKED, this.clickListener);
		slider.removeEventHandler(ScrollEvent.SCROLL, this.scrollListener);
		slider.removeEventHandler(KeyEvent.KEY_RELEASED, this.keyReleaseListener);
		slider.removeEventHandler(KeyEvent.KEY_PRESSED, this.keyPressListener);
	}

	private void pressed(MultiTouchHelper.Update update) {
		this.getSkinnable().touchedProperty().set(true);
		this.getSkinnable().requestFocus();
	}

	private void dragged(MultiTouchHelper.Update update) {

		SliderBase slider = this.getSkinnable();
		boolean vertical = this.isVertical(update);

		double range = vertical ? slider.getVerticalPixelRange() : slider.getHorizontalPixelRange();

		double deltaRatio = (vertical ? -update.deltaY : update.deltaX) / range;

		double shiftedDeltaRatio = this.shiftDown ? deltaRatio / 10 : deltaRatio;

		double newValue = slider.getCurve().changeExternalValueBy(slider.getValue(), shiftedDeltaRatio);

		slider.setValue(newValue);
	}

	private void released(MultiTouchHelper.Update update) {
		this.getSkinnable().touchedProperty().set(update.touchCount > 1);
	}

	// introduces a bit of inertia for switching between vertical and horizontal
	// movement evaluation
	private boolean isVertical(Update update) {

		int maxBias = 5;

		boolean vertical = update.isVerticalMovement();

		if (vertical && this.orientationBias < maxBias) {
			this.orientationBias++;
		} else if (!vertical && this.orientationBias > -maxBias) {
			this.orientationBias--;
		}

		return this.orientationBias >= 0;
	}

	private void keyPressed(KeyEvent e) {

		if (KeyCode.UP == e.getCode()) {
			this.shiftSliderValueBy(this.shiftDown ? 0.1 : 1, !this.shiftDown);
			e.consume();
		}

		if (KeyCode.DOWN == e.getCode()) {
			this.shiftSliderValueBy(this.shiftDown ? -0.1 : -1, !this.shiftDown);
			e.consume();
		}

		if (KeyCode.SHIFT == e.getCode()) {
			this.shiftDown = true;
			e.consume();
		}

	}

	private void keyReleased(KeyEvent e) {

		if (KeyCode.SHIFT == e.getCode()) {
			this.shiftDown = false;
			e.consume();
		}

	}

	private void scrolled(ScrollEvent e) {

		SliderBase slider = this.getSkinnable();

		if (slider.isTouched()) {
			return;
		}

		double pixelDeltaY = this.shiftDown ? e.getDeltaY() / 40 : e.getDeltaY() / 4;
		double deltaRatio = pixelDeltaY / slider.getVerticalPixelRange();
		double newValue = slider.getCurve().changeExternalValueBy(slider.getValue(), deltaRatio);
		slider.setValue(newValue);

		e.consume();
	}

	private void clicked(MouseEvent e) {

		if (e.getClickCount() != 2) {
			return;
		}

		SliderBase slider = this.getSkinnable();
		slider.setValue(slider.getDefaultValue());

		e.consume();
	}

	private void shiftSliderValueBy(double delta, boolean rasterize) {
		double currentValue = this.getSkinnable().getValue();
		double newValue = currentValue + delta;
		if (rasterize) {
			newValue = delta < 0 ? Math.ceil(newValue) : Math.floor(newValue);
		}
		this.getSkinnable().setValue(newValue);
	}

}
