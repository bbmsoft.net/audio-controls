package net.bbmsoft.controls.audio.scale;

import javafx.util.StringConverter;

public class FrequencyScaleConverter extends StringConverter<Double> {

	@Override
	public String toString(Double object) {

		double doubleValue = object.doubleValue();

		StringBuilder sb = new StringBuilder();

		if (doubleValue < 1_000) {
			sb.append(object.intValue()).append(" ");
		} else {
			sb.append(object.intValue() / 1000).append(" k");
		}

		sb.append("Hz");

		return sb.toString();
	}

	@Override
	public Double fromString(String string) {
		throw new UnsupportedOperationException();
	}

}
