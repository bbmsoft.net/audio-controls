package net.bbmsoft.controls.audio.utils;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Orientation;
import javafx.scene.layout.Region;

public class ScreenCurve extends LinearCurve {

	private final ObjectProperty<Orientation> orientation;
	private final Region region;

	public ScreenCurve(Region region, ObservableValue<Orientation> orientation) {
		this.region = region;
		this.orientation = new SimpleObjectProperty<Orientation>();
		this.orientation.bind(orientation);
		this.bind();
	}

	private void bind() {

		this.minProperty().bind(new SimpleDoubleProperty(0.0));
		this.orientation.addListener((o, ov, nv) -> this.updateOrientation(nv));
		this.updateOrientation(this.orientation.get());
		this.invertedProperty().bind(this.orientation.isEqualTo(Orientation.VERTICAL));
	}

	private void updateOrientation(Orientation orientation) {
		this.maxProperty()
				.bind(orientation == Orientation.VERTICAL ? this.region.heightProperty() : this.region.widthProperty());
	}

	public void dispose() {
		this.unbind();
	}

	private void unbind() {
		this.minProperty().unbind();
		this.maxProperty().unbind();
		this.orientation.unbind();
		this.invertedProperty().unbind();
	}
}
