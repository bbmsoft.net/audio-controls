package net.bbmsoft.controls.audio.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;
import javafx.util.Pair;

public class MultiTouchHelper {

	private static final int MOUSE_TOUCH_ID = -1;
	private static final int NO_TOUCH_ID = -2;

	private final Map<Integer, Pair<Double, Double>> lastTouchPoints;

	private final EventHandler<MouseEvent> mousePressed = this::mousePressed;
	private final EventHandler<MouseEvent> mouseReleased = this::mouseReleased;
	private final EventHandler<MouseEvent> mouseDragged = this::mouseDragged;

	private final EventHandler<TouchEvent> touchPressed = this::touchPressed;
	private final EventHandler<TouchEvent> touchReleased = this::touchReleased;
	private final EventHandler<TouchEvent> touchDragged = this::touchDragged;

	private Node client;

	private Consumer<Update> onTouchPressed;
	private Consumer<Update> onTouchReleased;
	private Consumer<Update> onTouchMoved;

	private int activeTouchID = NO_TOUCH_ID;
	
	private int mouseTouchPoints;

	public MultiTouchHelper(Node client) {
		this.client = client;
		this.lastTouchPoints = new HashMap<>();
		this.addListeners();
	}

	public void setOnTouchPressed(Consumer<Update> handler) {
		this.onTouchPressed = handler;
	}

	public void setOnTouchReleased(Consumer<Update> handler) {
		this.onTouchReleased = handler;
	}

	public void setOnTouchMoved(Consumer<Update> handler) {
		this.onTouchMoved = handler;
	}

	public void dispose() {
		this.removeListeners();
	}

	private void addListeners() {

		this.client.addEventHandler(MouseEvent.MOUSE_PRESSED, this.mousePressed);
		this.client.addEventHandler(MouseEvent.MOUSE_RELEASED, this.mouseReleased);
		this.client.addEventHandler(MouseEvent.MOUSE_DRAGGED, this.mouseDragged);

		this.client.addEventHandler(TouchEvent.TOUCH_PRESSED, this.touchPressed);
		this.client.addEventHandler(TouchEvent.TOUCH_RELEASED, this.touchReleased);
		this.client.addEventHandler(TouchEvent.TOUCH_MOVED, this.touchDragged);
	}

	private void removeListeners() {

		this.client.removeEventHandler(MouseEvent.MOUSE_PRESSED, this.mousePressed);
		this.client.removeEventHandler(MouseEvent.MOUSE_RELEASED, this.mouseReleased);
		this.client.removeEventHandler(MouseEvent.MOUSE_DRAGGED, this.mouseDragged);

		this.client.removeEventHandler(TouchEvent.TOUCH_PRESSED, this.touchPressed);
		this.client.removeEventHandler(TouchEvent.TOUCH_RELEASED, this.touchReleased);
		this.client.removeEventHandler(TouchEvent.TOUCH_MOVED, this.touchDragged);
	}

	private void mousePressed(MouseEvent e) {
		if (!e.isSynthesized()) {
			this.mouseTouchPoints++;
			this.touchPressed(MOUSE_TOUCH_ID, e.getX(), e.getY(), e.getScreenX(), e.getScreenY(), this.mouseTouchPoints);
		}
		e.consume();
	}

	private void mouseReleased(MouseEvent e) {
		if (!e.isSynthesized()) {
			this.touchReleased(MOUSE_TOUCH_ID, e.getX(), e.getY(), e.getScreenX(), e.getScreenY(), this.mouseTouchPoints);
			this.mouseTouchPoints--;
		}
		e.consume();
	}

	private void mouseDragged(MouseEvent e) {
		if (!e.isSynthesized()) {
			this.touchDragged(MOUSE_TOUCH_ID, e.getX(), e.getY(), e.getScreenX(), e.getScreenY(), this.mouseTouchPoints);
		}
		e.consume();
	}

	private void touchPressed(TouchEvent e) {
		this.touchPressed(e.getTouchPoint().getId(), e.getTouchPoint().getX(), e.getTouchPoint().getY(),
				e.getTouchPoint().getScreenX(), e.getTouchPoint().getScreenY(), e.getTouchCount());
		e.consume();
	}

	private void touchReleased(TouchEvent e) {
		this.touchReleased(e.getTouchPoint().getId(), e.getTouchPoint().getX(), e.getTouchPoint().getY(),
				e.getTouchPoint().getScreenX(), e.getTouchPoint().getScreenY(), e.getTouchCount());
		e.consume();
	}

	private void touchDragged(TouchEvent e) {
		this.touchDragged(e.getTouchPoint().getId(), e.getTouchPoint().getX(), e.getTouchPoint().getY(),
				e.getTouchPoint().getScreenX(), e.getTouchPoint().getScreenY(), e.getTouchCount());
		e.consume();

	}

	private void touchPressed(int touchID, double x, double y, double screenX, double screenY, int touchCount) {

		if (this.activeTouchID == NO_TOUCH_ID) {
			this.activeTouchID = touchID;
		}

		this.lastTouchPoints.put(touchID, new Pair<>(screenX, screenY));

		if (this.onTouchPressed != null) {
			this.onTouchPressed.accept(new Update(x, y, screenX, screenY, 0, 0, touchCount));
		}
	}

	private void touchReleased(int touchID, double x, double y, double screenX, double screenY, int touchCount) {

		this.lastTouchPoints.remove(touchID);

		if (this.activeTouchID == touchID) {
			this.activeTouchID = NO_TOUCH_ID;
		}

		if (this.onTouchReleased != null) {
			this.onTouchReleased.accept(new Update(x, y, screenX, screenY, 0, 0, touchCount));
		}
	}

	private void touchDragged(int touchID, double x, double y, double screenX, double screenY, int touchCount) {

		if (this.activeTouchID == NO_TOUCH_ID) {
			this.activeTouchID = touchID;
		}

		if (this.activeTouchID == touchID) {
			this.reportDrag(touchID, x, y, screenX, screenY, touchCount);
		}

		this.lastTouchPoints.put(touchID, new Pair<>(screenX, screenY));
	}

	private void reportDrag(int touchID, double x, double y, double screenX, double screenY, int touchCount) {

		if (this.onTouchMoved == null) {
			return;
		}

		Pair<Double, Double> lastTouchPoint = this.lastTouchPoints.get(touchID);
		if (lastTouchPoint == null) {
			return;
		}

		double deltaX = screenX - lastTouchPoint.getKey();
		double deltaY = screenY - lastTouchPoint.getValue();

		this.onTouchMoved.accept(new Update(x, y, screenX, screenY, deltaX, deltaY, touchCount));
	}

	public static class Update {

		public final double x;
		public final double y;

		public final double screenX;
		public final double screenY;

		public final double deltaX;
		public final double deltaY;

		public final int touchCount;

		public Update(double x, double y, double screenX, double screenY, double deltaX, double deltaY, int touchCount) {
			this.x = x;
			this.y = y;
			this.screenX = screenX;
			this.screenY = screenY;
			this.deltaX = deltaX;
			this.deltaY = deltaY;
			this.touchCount = touchCount;
		}

		public boolean isVerticalMovement() {

			double absDeltaX = Math.abs(deltaX);
			double absDeltaY = Math.abs(deltaY);

			return absDeltaY > absDeltaX;
		}

		public boolean isHorizontalMovement() {

			double absDeltaX = Math.abs(deltaX);
			double absDeltaY = Math.abs(deltaY);

			return absDeltaX > absDeltaY;
		}

	}
}
