package net.bbmsoft.controls.audio.eq;

import java.text.DecimalFormat;
import java.util.Objects;

import javafx.util.StringConverter;

public class FrequencyLabelConverter extends StringConverter<Double> {

	private final DecimalFormat format;

	public FrequencyLabelConverter(String format) {
		this.format = new DecimalFormat(Objects.requireNonNull(format));
	}

	public FrequencyLabelConverter() {
		this("0.#");
	}

	@Override
	public String toString(Double object) {

		double doubleValue = object.doubleValue();

		StringBuilder sb = new StringBuilder();

		if (doubleValue < 1_000) {
			sb.append(object.intValue()).append(" ");
		} else if (doubleValue < 10_000) {
			sb.append(this.format.format(Math.floor(doubleValue / 10) / 100)).append(" k");
		} else {
			sb.append(this.format.format(Math.floor(doubleValue / 100) / 10)).append(" k");
		}

		sb.append("Hz");

		return sb.toString();
	}

	@Override
	public Double fromString(String string) {

		String truncated = string;
		boolean thousand = false;

		if (string.endsWith(" Hz")) {
			truncated = string.substring(0, string.length() - 3);
		} else if (string.endsWith(" kHz")) {
			thousand = true;
			truncated = string.substring(0, string.length() - 4);
		}

		double value = Double.parseDouble(truncated);

		return thousand ? value * 1_000 : value;
	}

}
