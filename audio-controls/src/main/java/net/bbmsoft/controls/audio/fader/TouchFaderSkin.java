package net.bbmsoft.controls.audio.fader;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.SkinBase;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.StackPane;
import net.bbmsoft.controls.audio.Curve;
import net.bbmsoft.controls.audio.Scale;
import net.bbmsoft.controls.audio.impl.MultiTouchHelper;
import net.bbmsoft.controls.audio.utils.ScreenCurve;

public class TouchFaderSkin extends SkinBase<TouchFader> {

	private final StackPane track;
	private final StackPane thumb;

	private final DoubleProperty thumbOverlap;

	private final ScreenCurve screenCurve;

	private final InvalidationListener layoutListener;
	private final ChangeListener<Curve> curveListener;
	private final InvalidationListener scaleListener;

	private final EventHandler<KeyEvent> keyPressListener;
	private final EventHandler<KeyEvent> keyReleaseListener;
	private final EventHandler<ScrollEvent> scrollListener;
	private final EventHandler<MouseEvent> clickListener;
	private final EventHandler<MouseEvent> pressListener;

	private MultiTouchHelper mth;

	private boolean shiftDown;

	public TouchFaderSkin(TouchFader fader) {

		super(fader);

		this.track = new StackPane();
		this.thumb = new StackPane();

		this.thumbOverlap = new SimpleDoubleProperty(this.getSkinnable(), "thumb-overlap", 32);

		this.screenCurve = new ScreenCurve(fader, fader.orientationProperty());

		this.layoutListener = o -> Platform.runLater(() -> fader.requestLayout());
		this.scaleListener = o -> this.updateScaleInsets();
		this.curveListener = (o, ov, nv) -> {
			this.removeListeners(fader, ov);
			this.addListeners(fader, nv);
		};
		this.keyPressListener = this::keyPressed;
		this.keyReleaseListener = this::keyReleased;
		this.scrollListener = this::scrolled;
		this.clickListener = this::clicked;
		this.pressListener = this::pressed;

		fader.getStyleClass().add("fader");
		this.track.getStyleClass().add("track");
		this.thumb.getStyleClass().add("thumb");

		this.getChildren().addAll(this.track, this.thumb);

		this.updateScaleInsets();

		this.addListeners(fader, fader.getCurve());
	}

	@Override
	public void dispose() {

		TouchFader fader = this.getSkinnable();

		this.removeListeners(fader, fader.getCurve());

		fader.getStyleClass().remove("fader");
		this.getChildren().removeAll(track, thumb);
		this.screenCurve.dispose();
		super.dispose();
	}

	protected void layoutInArea(Node child, double areaX, double areaY, double areaWidth, double areaHeight,
			double areaBaselineOffset, Insets margin, boolean fillWidth, boolean fillHeight, HPos halignment,
			VPos valignment) {

		TouchFader fader = this.getSkinnable();
		Orientation orientation = fader.getOrientation();
		boolean vertical = orientation == Orientation.VERTICAL;

		if (child == this.track) {

			if (vertical) {

				double prefWidth = Math.max(0, this.track.getPrefWidth()) + this.track.getInsets().getLeft()
						+ this.track.getInsets().getRight();

				this.track.resize(prefWidth, areaHeight - this.thumbOverlap.get());
				this.track.relocate(areaX + (areaWidth - prefWidth) / 2, areaY + this.thumbOverlap.get() / 2);

			} else {

				double prefHeight = Math.max(0, this.track.getPrefHeight()) + this.track.getInsets().getTop()
						+ this.track.getInsets().getBottom();

				this.track.resize(areaWidth - this.thumbOverlap.get(), prefHeight);
				this.track.relocate(areaX + this.thumbOverlap.get() / 2, areaY + (areaHeight - prefHeight) / 2);
			}

		} else if (child == this.thumb) {

			double prefWidth = Math.max(0, this.thumb.getPrefWidth()) + this.thumb.getInsets().getLeft()
					+ this.thumb.getInsets().getRight();

			double prefHeight = Math.max(0, this.thumb.getPrefHeight()) + this.thumb.getInsets().getTop()
					+ this.thumb.getInsets().getBottom();

			double posX = areaX + (vertical ? (areaWidth - this.thumb.getWidth()) / 2
					: this.calcThumbX(fader, areaWidth, prefWidth));
			double posY = areaY + (vertical ? this.calcThumbY(fader, areaHeight, prefHeight)
					: (areaHeight - this.thumb.getHeight()) / 2);

			this.thumb.resize(prefWidth, prefHeight);
			this.thumb.relocate(posX, posY);
		}

	}

	private void addListeners(TouchFader fader, Curve curve) {

		fader.curveProperty().addListener(this.curveListener);
		
		this.thumb.widthProperty().addListener(this.layoutListener);
		this.thumb.heightProperty().addListener(this.layoutListener);
		this.track.widthProperty().addListener(this.layoutListener);
		this.track.heightProperty().addListener(this.layoutListener);

		curve.minProperty().addListener(this.layoutListener);
		curve.maxProperty().addListener(this.layoutListener);
		fader.valueProperty().addListener(this.layoutListener);
		fader.scaleProperty().addListener(this.scaleListener);

		this.mth = new MultiTouchHelper(this.thumb);
		this.mth.setOnTouchPressed(this::thumbPressed);
		this.mth.setOnTouchMoved(this::thumbDragged);
		this.mth.setOnTouchReleased(this::thumbReleased);

		this.thumb.widthProperty().addListener(o -> this.updateScaleInsets());
		this.thumb.heightProperty().addListener(o -> this.updateScaleInsets());

		fader.addEventHandler(KeyEvent.KEY_PRESSED, this.keyPressListener);
		fader.addEventHandler(KeyEvent.KEY_RELEASED, this.keyReleaseListener);
		fader.addEventHandler(ScrollEvent.SCROLL, this.scrollListener);
		fader.addEventHandler(MouseEvent.MOUSE_CLICKED, this.clickListener);
		fader.addEventHandler(MouseEvent.MOUSE_CLICKED, this.pressListener);
	}

	private void removeListeners(TouchFader fader, Curve curve) {

		fader.removeEventHandler(MouseEvent.MOUSE_CLICKED, this.pressListener);
		fader.removeEventHandler(MouseEvent.MOUSE_CLICKED, this.clickListener);
		fader.removeEventHandler(ScrollEvent.SCROLL, this.scrollListener);
		fader.removeEventHandler(KeyEvent.KEY_RELEASED, this.keyReleaseListener);
		fader.removeEventHandler(KeyEvent.KEY_PRESSED, this.keyPressListener);

		this.mth.dispose();

		fader.curveProperty().removeListener(this.curveListener);

		curve.minProperty().removeListener(this.layoutListener);
		curve.maxProperty().removeListener(this.layoutListener);
		fader.valueProperty().removeListener(this.layoutListener);
		fader.scaleProperty().removeListener(this.scaleListener);
	}

	private double calcThumbX(TouchFader fader, double areaWidth, double thumbWidth) {

		double ratio = fader.getRelativeValue();
		return ratio * areaWidth - ratio * thumbWidth;
	}

	private double calcThumbY(TouchFader fader, double areaHeight, double thumbHeight) {

		double ratio = fader.getRelativeValue();
		return (1 - ratio) * areaHeight - (1 - ratio) * thumbHeight;
	}

	private void thumbPressed(MultiTouchHelper.Update update) {
		this.getSkinnable().touchedProperty().set(true);
		this.getSkinnable().requestFocus();
	}

	private void thumbDragged(MultiTouchHelper.Update update) {

		TouchFader fader = this.getSkinnable();
		boolean vertical = fader.getOrientation() == Orientation.VERTICAL;

		// TODO insets?
		double range = vertical ? fader.getHeight() - this.thumb.getHeight() : fader.getWidth() - this.thumb.getWidth();

		double deltaRatio = (vertical ? -update.deltaY : update.deltaX) / range;

		double shiftedDeltaRatio = this.shiftDown ? deltaRatio / 10 : deltaRatio;

		double newValue = fader.getCurve().changeExternalValueBy(fader.getValue(), shiftedDeltaRatio);

		fader.setValue(newValue);
	}

	private void thumbReleased(MultiTouchHelper.Update update) {
		this.getSkinnable().touchedProperty().set(update.touchCount > 1);
	}

	private void updateScaleInsets() {

		TouchFader fader = this.getSkinnable();
		Scale scale = fader.getScale();

		if (scale == null) {
			return;
		}

		scale.topInsetProperty().set(this.calcTopInset());
		scale.rightInsetProperty().set(this.calcRightInset());
		scale.bottomInsetProperty().set(this.calcBottomInset());
		scale.leftInsetProperty().set(this.calcLeftInset());
	}

	private double calcTopInset() {
		TouchFader fader = this.getSkinnable();
		return (fader.getOrientation() == Orientation.VERTICAL ? this.thumb.getHeight() / 2 - 1 : 0.0)
				+ fader.getInsets().getTop();
	}

	private double calcRightInset() {
		TouchFader fader = this.getSkinnable();
		return (fader.getOrientation() == Orientation.HORIZONTAL ? this.thumb.getWidth() / 2 : 0.0)
				+ fader.getInsets().getRight();
	}

	private double calcBottomInset() {
		TouchFader fader = this.getSkinnable();
		return (fader.getOrientation() == Orientation.VERTICAL ? this.thumb.getHeight() / 2 : 0.0)
				+ fader.getInsets().getBottom();
	}

	private double calcLeftInset() {
		TouchFader fader = this.getSkinnable();
		return (fader.getOrientation() == Orientation.HORIZONTAL ? this.thumb.getWidth() / 2 : 0.0)
				+ fader.getInsets().getLeft();
	}

	private void keyPressed(KeyEvent e) {

		if (KeyCode.UP == e.getCode()) {
			this.shiftFaderValueBy(this.shiftDown ? 0.1 : 1, !this.shiftDown);
			e.consume();
		}

		if (KeyCode.DOWN == e.getCode()) {
			this.shiftFaderValueBy(this.shiftDown ? -0.1 : -1, !this.shiftDown);
			e.consume();
		}

		if (KeyCode.SHIFT == e.getCode()) {
			this.shiftDown = true;
			e.consume();
		}

	}

	private void keyReleased(KeyEvent e) {

		if (KeyCode.SHIFT == e.getCode()) {
			this.shiftDown = false;
			e.consume();
		}

	}

	private void scrolled(ScrollEvent e) {

		if (this.getSkinnable().isTouched()) {
			return;
		}

		double pixelDeltaY = this.shiftDown ? e.getDeltaY() / 40 : e.getDeltaY() / 4;
		double deltaRatio = pixelDeltaY / this.screenCurve.getRange();
		double newValue = this.getSkinnable().getCurve().changeExternalValueBy(this.getSkinnable().getValue(),
				deltaRatio);
		this.getSkinnable().setValue(newValue);

		e.consume();
	}

	private void pressed(MouseEvent e) {
		this.getSkinnable().requestFocus();
		e.consume();
	}

	private void clicked(MouseEvent e) {

		if (e.getClickCount() != 2) {
			return;
		}

		TouchFader fader = this.getSkinnable();

		if (fader.getOrientation() == Orientation.VERTICAL) {
			double y = e.getY();
			double thumbY = this.thumb.getLayoutY() + this.thumb.getHeight() / 2;

			if (y > thumbY) {
				fader.setValue(fader.getMin());
			} else {
				fader.setValue(0.0);
			}
		} else {
			double x = e.getX();
			double thumbX = this.thumb.getLayoutX() + this.thumb.getWidth() / 2;

			if (thumbX > x) {
				fader.setValue(fader.getMin());
			} else {
				fader.setValue(0.0);
			}
		}

		e.consume();
	}

	private void shiftFaderValueBy(double delta, boolean rasterize) {
		double currentValue = this.getSkinnable().getValue();
		double newValue = currentValue + delta;
		if (rasterize) {
			newValue = delta < 0 ? Math.ceil(newValue) : Math.floor(newValue);
		}
		this.getSkinnable().setValue(newValue);
	}

}
