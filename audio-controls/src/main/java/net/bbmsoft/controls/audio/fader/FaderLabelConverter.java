package net.bbmsoft.controls.audio.fader;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import net.bbmsoft.controls.audio.slider.SliderLabelConverter;

public class FaderLabelConverter extends SliderLabelConverter {

	private final DoubleProperty negativeInifity;

	public FaderLabelConverter() {
		this.negativeInifity = new SimpleDoubleProperty(this, "negative-infinity", -Double.MAX_VALUE);
	}

	public DoubleProperty negativeInfinityProperty() {
		return this.negativeInifity;
	}

	public double getNegativeInfinity() {
		return this.negativeInifity.get();
	}

	public void setNegativeInfinity(double value) {
		this.negativeInifity.set(value);
	}

	@Override
	public String toString(Double tick) {

		if (tick <= this.negativeInifity.get()) {
			return "-∞";
		}

		return super.toString(tick);
	}

	@Override
	public Double fromString(String string) {

		if (string.equals("-∞")) {
			return Double.valueOf(this.negativeInifity.get());
		} else {
			return super.fromString(string);
		}
	}
}
