package net.bbmsoft.controls.audio.eq;

import java.text.DecimalFormat;
import java.text.ParseException;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.util.StringConverter;

public class GainTextFieldConverter extends StringConverter<Double> {

	private final DecimalFormat format;
	private final DoubleProperty negativeInfinity;

	public GainTextFieldConverter() {
		this(true);
	}

	public GainTextFieldConverter(boolean showUnit) {
		this(showUnit, new SimpleDoubleProperty(-Double.MAX_VALUE));
	}
	
	public GainTextFieldConverter(boolean showUnit, DoubleProperty negativeInfinity) {
		this.negativeInfinity = negativeInfinity;
		this.format = new DecimalFormat(showUnit ? "0.0 dB" : "0.0");
	}

	@Override
	public String toString(Double object) {

		double doubleValue = object.doubleValue();
		
		if(this.negativeInfinity != null && doubleValue <= this.negativeInfinity.getValue().doubleValue()) {
			return "-∞";
		}

		return doubleValue > 0.0 ? "+" + this.format.format(doubleValue) : this.format.format(doubleValue);
	}

	@Override
	public Double fromString(String string) {

		String trim = string.trim();

		if (trim.endsWith("dB")) {
			try {
				return this.format.parse(string).doubleValue();
			} catch (ParseException e) {
				throw new NumberFormatException(e.getMessage());
			}
		} else {
			return Double.parseDouble(trim);
		}
	}

	public final DoubleProperty negativeInfinityProperty() {
		return this.negativeInfinity;
	}
	

	public final double getNegativeInfinity() {
		return this.negativeInfinityProperty().get();
	}
	

	public final void setNegativeInfinity(final double negativeInfinity) {
		this.negativeInfinityProperty().set(negativeInfinity);
	}
	
}
