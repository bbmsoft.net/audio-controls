package net.bbmsoft.controls.audio.fader;

import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Skin;
import javafx.scene.text.TextAlignment;
import javafx.util.StringConverter;
import net.bbmsoft.controls.audio.Fader;
import net.bbmsoft.controls.audio.Scale;
import net.bbmsoft.controls.audio.eq.GainTextFieldConverter;
import net.bbmsoft.controls.audio.impl.SingleAxisControlBase;

public class TouchFader extends SingleAxisControlBase implements Fader {

	public TouchFader() {
		super(new GainTextFieldConverter(false));
		((GainTextFieldConverter) this.getTextFieldConverter()).negativeInfinityProperty().bind(this.minProperty());
		this.setTickLabelConverter(new FaderLabelConverter());
		this.setLabelConverter(new FaderLabelConverter());
	}

	@Override
	protected Skin<?> createDefaultSkin() {
		return new TouchFaderSkin(this);
	}

	@Override
	public String getUserAgentStylesheet() {
		return this.getClass().getResource("fader.css").toExternalForm();
	}
	
	@Override
	protected void updateScale(Scale oldScale, Scale newScale) {
		super.updateScale(oldScale, newScale);
		
		if(newScale != null) {
			newScale.alignmentProperty().set(TextAlignment.CENTER);
			newScale.hPosProperty().set(HPos.LEFT);
			newScale.vPosProperty().set(VPos.TOP);
		}
	}
	
	protected void updateTickLabelConverter(StringConverter<Double> ov, StringConverter<Double> nv) {

		if (ov instanceof FaderLabelConverter) {
			((FaderLabelConverter) ov).negativeInfinityProperty().unbind();
		}

		if (nv instanceof FaderLabelConverter) {
			((FaderLabelConverter) nv).negativeInfinityProperty().bind(this.minProperty());
		}
	}

	protected void updateLabelConverter(StringConverter<Double> ov, StringConverter<Double> nv) {

		if (ov instanceof FaderLabelConverter) {
			((FaderLabelConverter) ov).negativeInfinityProperty().unbind();
		}

		if (nv instanceof FaderLabelConverter) {
			((FaderLabelConverter) nv).negativeInfinityProperty().bind(this.minProperty());
		}
	}
}
