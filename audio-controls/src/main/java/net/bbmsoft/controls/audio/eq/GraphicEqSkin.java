package net.bbmsoft.controls.audio.eq;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import net.bbmsoft.controls.audio.EQ.Band;

public class GraphicEqSkin extends SkinBase<EqBase> {

	private final InvalidationListener bandCountListener;
	private final HBox container;

	private Runnable faderUpdate;

	public GraphicEqSkin(EqBase eq) {
		super(eq);
		this.bandCountListener = o -> this.updateFaders();
		this.container = new HBox();
		this.container.getStyleClass().add("container");
		this.container.setSpacing(8);
		this.addListeners(eq);
		this.updateFaders();
		this.getChildren().setAll(this.container);
	}

	@Override
	public void dispose() {
		this.removeListeners(this.getSkinnable());
		super.dispose();
	}

	private void addListeners(EqBase eq) {
		eq.getBands().addListener(this.bandCountListener);
	}

	private void removeListeners(EqBase eq) {
		eq.getBands().removeListener(this.bandCountListener);
	}

	private void updateFaders() {

		if (this.faderUpdate == null) {
			Platform.runLater(this.faderUpdate = this::doUpdateFaders);
		}
	}

	private void doUpdateFaders() {

		this.faderUpdate = null;

		List<GraphicEqBand> bands = new ArrayList<>();

		EqBase eq = this.getSkinnable();
		
		for (Band band : eq.getBands()) {
			bands.add(new GraphicEqBand(eq, band));
		}

		this.container.getChildren().setAll(bands);
	}

}
