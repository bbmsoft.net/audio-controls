package net.bbmsoft.controls.audio.eq;

import javafx.scene.control.Skin;

public class ParametricEQ extends EqBase {

	@Override
	protected Skin<?> createDefaultSkin() {
		return new ParametricEqSkin(this);
	}
	
	@Override
	public String getUserAgentStylesheet() {

		return this.getClass().getResource("peq.css").toExternalForm();
	}
}
