package net.bbmsoft.controls.audio.eq;

import static java.lang.Math.sqrt;
import static net.bbmsoft.controls.audio.eq.CurveCoeffs.coeffs;

import net.bbmsoft.controls.audio.EQ.Band;

public class LowPassCurvePlotter implements BandCurvePlotter {

	private double slope;

	public LowPassCurvePlotter(double slope) {
		this.slope = slope;
	}

	@Override
	public double computeGain(double frequency, Band band) {

		double f = band.getFrequency();

		double f0 = f / frequency;
		double f1 = square(1 / f0);
		double f2 = square(f1);
		double d = 1;

		int order = (int) (slope / 6);
		int ordOff = order == 0 ? 1 : order;

		for (int k = 0; k < (order + 1) / 2; k++) {
			double a = coeffs[ordOff - 1][k];
			double b = coeffs[ordOff - 1][k + 3];
			d *= (1 + (square(a) - 2 * b) * f1 + square(b) * f2);
		}

		double pOut = sqrt(1 / d);
		double gainOut = this.toDB(pOut);

		return gainOut;
	}
}
