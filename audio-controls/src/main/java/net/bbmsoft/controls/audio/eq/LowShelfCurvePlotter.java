package net.bbmsoft.controls.audio.eq;

import static java.lang.Math.sqrt;

import net.bbmsoft.controls.audio.EQ.Band;

public class LowShelfCurvePlotter implements BandCurvePlotter {

	@Override
	public double computeGain(double frequency, Band band) {

		double f = band.getFrequency();
		double p = this.toPower(band.getGain());
		double pR = this.toPr(p);

		double f0 = f / frequency;
		double f1 = square(f0);
		double f2 = square(1 - f1);

		double d = f2 + 2 * f1;
		double n;
		double pOut;

		if (p >= 1) {
			n = square(1 - p * f1) + (2 * p) * f1;
			pOut = sqrt(n / d);
		} else {
			n = square(1 - pR * f1) + (2 * pR) * f1;
			pOut = sqrt(d / n);
		}

		double gainOut = this.toDB(pOut);

		return gainOut;
	}
}
