package net.bbmsoft.controls.audio.eq;

import static java.lang.Math.log10;

import net.bbmsoft.controls.audio.EQ;

public interface BandCurvePlotter {

	public default double toPower(double gain) {
		double p = Math.pow(10, gain / 20);
		return p;
	}

	public default double toDB(double power) {
		double dB = 20 * log10(power);
		return dB;
	}

	public default double toPr(double p) {
		double pR = p >= 1 ? p : 1 / p;
		return pR;
	}

	public default double square(double x) {
		return x * x;
	}

	public abstract double computeGain(double frequency, EQ.Band band);

}
