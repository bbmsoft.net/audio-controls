package net.bbmsoft.controls.audio.slider;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import net.bbmsoft.controls.audio.Curve;
import net.bbmsoft.controls.audio.Slider;
import net.bbmsoft.controls.audio.impl.TouchControlBase;
import net.bbmsoft.controls.audio.utils.LinearCurve;

public class SliderBase extends TouchControlBase implements Slider {

	private final DoubleProperty value;
	private final DoubleProperty min;
	private final DoubleProperty max;
	private final DoubleProperty verticalPixelRange;
	private final DoubleProperty horizontalPixelRange;
	private final DoubleProperty defaultValue;
	private final ObjectProperty<Curve> curve;
	private final BooleanProperty inverted;
	private final ObjectProperty<TextField> textInput;
	private final ObjectProperty<StringConverter<Double>> textInputConverter;

	private final EventHandler<ActionEvent> textListener;

	public SliderBase() {

		this.defaultValue = new SimpleDoubleProperty(this, "default-value", 0.0);

		this.value = new SimpleDoubleProperty(this, "value", this.getDefaultValue()) {
			@Override
			protected void invalidated() {
				double value = this.get();
				double max = getMax();
				double min = getMin();
				if (value > max || value < min) {
					this.set(clamp(min, value, max));
				}
				requestLayout();
				updateTextInput(this.getValue());
			}
		};

		this.min = new SimpleDoubleProperty(this, "min", 0.0) {
			@Override
			protected void invalidated() {
				double value = valueProperty().get();
				double min = this.get();
				if (value < min) {
					valueProperty().set(min);
				}
				requestLayout();
			}
		};

		this.max = new SimpleDoubleProperty(this, "max", 1.0) {
			@Override
			protected void invalidated() {
				double value = valueProperty().get();
				double max = this.get();
				if (value > max) {
					valueProperty().set(max);
				}
				requestLayout();
			}
		};

		this.verticalPixelRange = new SimpleDoubleProperty(this, "vertical-pixel-range", 500) {
			@Override
			protected void invalidated() {
				if (this.get() < 10) {
					this.set(10);
				}
			}
		};

		this.horizontalPixelRange = new SimpleDoubleProperty(this, "horizontal-pixel-range", 1500) {
			@Override
			protected void invalidated() {
				if (this.get() < 10) {
					this.set(10);
				}
			}
		};

		this.inverted = new SimpleBooleanProperty(this, "inverted", false);
		this.curve = new SimpleObjectProperty<Curve>(this, "curve", null);
		this.textInput = new SimpleObjectProperty<TextField>(this, "text-input", null);
		this.textInputConverter = new SimpleObjectProperty<StringConverter<Double>>(this, "tick-label-converter", null);
		this.textListener = this::textInputUpdated;
		this.textInput.addListener((o, ov, nv) -> {
			if (ov != null) {
				this.disconnect(ov);
			}
			if (nv != null) {
				this.connect(nv);
			}
		});

		this.curve.addListener((o, ov, nv) -> this.curveChanged(ov, nv));

		this.setCurve(new LinearCurve());
		
		this.getStyleClass().add("slider");
	}

	@Override
	public DoubleProperty verticalPixelRangeProperty() {
		return verticalPixelRange;
	}

	@Override
	public double getVerticalPixelRange() {
		return this.verticalPixelRange.get();
	}

	@Override
	public void setVerticalPixelRange(double value) {
		this.verticalPixelRange.set(value);
	}

	@Override
	public DoubleProperty horizontalPixelRangeProperty() {
		return this.horizontalPixelRange;
	}

	@Override
	public double getHorizontalPixelRange() {
		return this.horizontalPixelRange.get();
	}

	@Override
	public void setHorizontalPixelRange(double value) {
		this.horizontalPixelRange.set(value);
	}

	@Override
	public DoubleProperty valueProperty() {
		return value;
	}

	@Override
	public double getValue() {
		return this.value.get();
	}

	@Override
	public void setValue(double value) {
		this.value.set(value);
	}

	@Override
	public DoubleProperty minProperty() {
		return this.min;
	}

	@Override
	public double getMin() {
		return this.min.get();
	}

	@Override
	public void setMin(double value) {
		this.min.set(value);
	}

	@Override
	public DoubleProperty maxProperty() {
		return this.max;
	}

	@Override
	public double getMax() {
		return this.max.get();
	}

	@Override
	public void setMax(double value) {
		this.max.set(value);
	}

	@Override
	public DoubleProperty defaultValueProperty() {
		return this.defaultValue;
	}

	@Override
	public double getDefaultValue() {
		return this.defaultValue.get();
	}

	@Override
	public void setDefaultValue(double value) {
		this.defaultValue.set(value);
	}

	@Override
	public ObjectProperty<Curve> curveProperty() {
		return this.curve;
	}

	@Override
	public Curve getCurve() {
		return this.curve.get();
	}

	@Override
	public void setCurve(Curve value) {
		this.curve.set(value);
	}

	@Override
	public BooleanProperty invertedProperty() {
		return this.inverted;
	}

	@Override
	public boolean isInverted() {
		return this.inverted.get();
	}

	@Override
	public void setInverted(boolean value) {
		this.inverted.set(value);
	}

	@Override
	public ObjectProperty<TextField> textInputProperty() {
		return this.textInput;
	}

	@Override
	public TextField getTextInput() {
		return this.textInput.get();
	}

	@Override
	public void setTextInput(TextField value) {
		this.textInput.set(value);
	}

	@Override
	public ObjectProperty<StringConverter<Double>> textInputConverterProperty() {
		return this.textInputConverter;
	}

	@Override
	public StringConverter<Double> getTextInputConverter() {
		return this.textInputConverter.get();
	}

	@Override
	public void setTextInputConverter(StringConverter<Double> value) {
		this.textInputConverter.set(value);
	}

	private void curveChanged(Curve ov, Curve nv) {

		if (ov != null) {
			ov.minProperty().unbind();
			ov.maxProperty().unbind();
			ov.invertedProperty().unbind();
		}

		if (nv != null) {
			nv.minProperty().bind(this.min);
			nv.maxProperty().bind(this.max);
			nv.invertedProperty().bind(this.inverted);
		}

		this.requestLayout();
	}

	private void connect(TextField textInput) {

		textInput.addEventHandler(ActionEvent.ACTION, this.textListener);
		this.updateTextInput(this.getValue());
	}

	private void disconnect(TextField textInput) {
		textInput.removeEventHandler(ActionEvent.ACTION, this.textListener);
	}

	private void updateTextInput(Double newValue) {

		TextField TextField = this.textInput.get();

		if (TextField == null) {
			return;
		}

		StringConverter<Double> tickLabelConverter = this.getTextInputConverter();

		if (tickLabelConverter != null) {
			TextField.setText(tickLabelConverter.toString(newValue));
		} else {
			TextField.setText(String.valueOf(newValue));
		}
	}

	private void textInputUpdated(ActionEvent e) {

		if (this.isTouched()) {
			return;
		}

		String text = ((TextField) e.getSource()).getText();

		try {
			double newValue;

			StringConverter<Double> tickLabelConverter = this.getTextInputConverter();

			if (tickLabelConverter != null) {
				newValue = tickLabelConverter.fromString(text);
			} else {
				newValue = Double.parseDouble(text);
			}

			this.setValue(newValue);

		} catch (NumberFormatException ex) {
			Platform.runLater(() -> this.updateTextInput(this.getValue()));
		}
	}
}
