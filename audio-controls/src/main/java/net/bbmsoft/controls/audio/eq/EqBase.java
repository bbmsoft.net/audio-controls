package net.bbmsoft.controls.audio.eq;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import net.bbmsoft.controls.audio.Curve;
import net.bbmsoft.controls.audio.EQ;
import net.bbmsoft.controls.audio.impl.TouchControlBase;
import net.bbmsoft.controls.audio.utils.LinearCurve;
import net.bbmsoft.controls.audio.utils.LogarithmicCurve;

public class EqBase extends TouchControlBase implements EQ {

	private final PseudoClass activePseudoClass = PseudoClass.getPseudoClass("bypassed");

	private final IntegerProperty bandCount;
	private final ObservableList<Band> bands;

	private final DoubleProperty minFrequency;
	private final DoubleProperty maxFrequency;
	private final DoubleProperty minGain;
	private final DoubleProperty maxGain;
	private final DoubleProperty minQ;
	private final DoubleProperty maxQ;
	private final BooleanProperty active;

	private final ObjectProperty<Curve> frequencyCurve;
	private final ObjectProperty<Curve> gainCurve;

	private boolean updatingBands;

	public EqBase() {

		this.bandCount = new SimpleIntegerProperty(this, "band-count", 0);
		this.bands = FXCollections.observableArrayList();

		this.minFrequency = new SimpleDoubleProperty(this, "min-frequency", 20.0);
		this.maxFrequency = new SimpleDoubleProperty(this, "max-frequency", 24_000.0);

		this.minGain = new SimpleDoubleProperty(this, "min-gain", -24.0);
		this.maxGain = new SimpleDoubleProperty(this, "max-gain", 24.0);

		this.minQ = new SimpleDoubleProperty(this, "min-q", 0.01);
		this.maxQ = new SimpleDoubleProperty(this, "max-q", 100.0);

		this.active = new SimpleBooleanProperty(this, "active", false) {
			@Override
			protected void invalidated() {
				pseudoClassStateChanged(activePseudoClass, !this.get());
			}
		};

		this.frequencyCurve = new SimpleObjectProperty<>(this, "frequency-curve", null);
		this.frequencyCurve.addListener((o, ov, nv) -> this.frequencyCurveChanged(ov, nv));
		this.gainCurve = new SimpleObjectProperty<>(this, "gain-curve", null);
		this.gainCurve.addListener((o, ov, nv) -> this.gainCurveChanged(ov, nv));

		this.setFrequencyCurve(new LogarithmicCurve());
		this.setGainCurve(new LinearCurve());

		this.bandCount.addListener((o, ov, nv) -> this.updateBandCount(nv.intValue()));
		this.bands.addListener((Observable o) -> {
			if (this.updatingBands) {
				return;
			}
			this.bandCount.set(this.bands.size());
		});

		this.getStyleClass().add("eq");
	}

	@Override
	public IntegerProperty bandCountProperty() {
		return bandCount;
	}

	@Override
	public void setBandCount(int value) {
		this.bandCount.set(value);
	}

	@Override
	public int getBandCount() {
		return this.bandCount.get();
	}

	@Override
	public ObservableList<Band> getBands() {
		return this.bands;
	}

	@Override
	public DoubleProperty minGainProperty() {
		return this.minGain;
	}

	@Override
	public double getMinGain() {
		return this.minGain.get();
	}

	@Override
	public void setMinGain(double value) {
		this.minGain.set(value);
	}

	@Override
	public DoubleProperty maxGainProperty() {
		return this.maxGain;
	}

	@Override
	public double getMaxGain() {
		return this.maxGain.get();
	}

	@Override
	public void setMaxGain(double value) {
		this.maxGain.set(value);
	}

	@Override
	public DoubleProperty minFrequencyProperty() {
		return this.minFrequency;
	}

	@Override
	public double getMinFrequency() {
		return this.minFrequency.get();
	}

	@Override
	public void setMinFrequency(double value) {
		this.minFrequency.set(value);
	}

	@Override
	public DoubleProperty maxFrequencyProperty() {
		return this.maxFrequency;
	}

	@Override
	public double getMaxFrequency() {
		return this.maxFrequency.get();
	}

	@Override
	public void setMaxFrequency(double value) {
		this.maxFrequency.set(value);
	}

	@Override
	public DoubleProperty minQProperty() {
		return this.minQ;
	}

	@Override
	public double getMinQ() {
		return this.minQ.get();
	}

	@Override
	public void setMinQ(double value) {
		this.minQ.set(value);
	}

	@Override
	public DoubleProperty maxQProperty() {
		return this.maxQ;
	}

	@Override
	public double getMaxQ() {
		return this.maxQ.get();
	}

	@Override
	public void setMaxQ(double value) {
		this.maxQ.set(value);
	}

	@Override
	public ObjectProperty<Curve> frequencyCurveProperty() {
		return this.frequencyCurve;
	}

	@Override
	public Curve getFrequencyCurve() {
		return this.frequencyCurve.get();
	}

	@Override
	public void setFrequencyCurve(Curve value) {
		this.frequencyCurve.set(value);
	}

	@Override
	public ObjectProperty<Curve> gainCurveProperty() {
		return this.gainCurve;
	}

	@Override
	public Curve getGainCurve() {
		return this.gainCurve.get();
	}

	@Override
	public void setGainCurve(Curve value) {
		this.gainCurve.set(value);
	}

	@Override
	public BooleanProperty activeProperty() {
		return this.active;
	}

	@Override
	public boolean isActive() {
		return this.active.get();
	}

	@Override
	public void setActive(boolean value) {
		this.active.set(value);
	}

	@Override
	public void reset() {

		double d = 1.0 / this.bands.size();

		for (int i = 0; i < this.bands.size(); i++) {

			Band band = this.bands.get(i);

			if (!band.frequencyProperty().isBound()) {
				band.setFrequency(this.getFrequencyCurve().toExternalValue((i + 0.5) * d));
			}
			if (!band.gainProperty().isBound()) {
				band.setGain(0.0);
			}
			if (!band.qProperty().isBound()) {
				band.setQ(1.0);
			}
		}
	}

	private void updateBandCount(int newCount) {

		this.updatingBands = true;

		int diff = newCount - this.bands.size();

		if (diff > 0) {
			this.addBands(diff);
		} else if (diff < 0) {
			this.removeBands(-diff);
		}

		this.updatingBands = false;

	}

	private void addBands(int diff) {

		List<Band> newBands = new ArrayList<>();
		newBands.addAll(this.getBands());

		for (int i = 0; i < diff; i++) {
			newBands.add(new EqBand(this));
		}

		this.bands.setAll(newBands);

		if (this.updatingBands) {
			this.reset();
		}

	}

	private void removeBands(int diff) {
		List<Band> toBeRemoved = new ArrayList<>(this.bands.subList(0, diff));
		this.bands.removeAll(toBeRemoved);
	}

	private void frequencyCurveChanged(Curve ov, Curve nv) {

		if (ov != null) {
			ov.minProperty().unbind();
			ov.maxProperty().unbind();
			ov.invertedProperty().unbind();
		}

		if (nv != null) {
			nv.minProperty().bind(this.minFrequency);
			nv.maxProperty().bind(this.maxFrequency);
			nv.invertedProperty().bind(new SimpleBooleanProperty(false));
		}

		this.requestLayout();
	}

	private void gainCurveChanged(Curve ov, Curve nv) {

		if (ov != null) {
			ov.minProperty().unbind();
			ov.maxProperty().unbind();
			ov.invertedProperty().unbind();
		}

		if (nv != null) {
			nv.minProperty().bind(this.minGain);
			nv.maxProperty().bind(this.maxGain);
			nv.invertedProperty().bind(new SimpleBooleanProperty(false));
		}

		this.requestLayout();
	}

}
