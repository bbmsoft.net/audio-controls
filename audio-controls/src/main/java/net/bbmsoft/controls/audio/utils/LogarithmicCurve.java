package net.bbmsoft.controls.audio.utils;

import javafx.beans.value.ObservableValue;

public class LogarithmicCurve extends CurveBase {

	public LogarithmicCurve() {
		super();
	}

	public LogarithmicCurve(double min, double max, boolean inverted) {
		super(min, max, inverted);
	}

	public LogarithmicCurve(ObservableValue<Number> min, ObservableValue<Number> max,
			ObservableValue<Boolean> inverted) {
		super(min, max, inverted);
	}

	@Override
	protected double toUnclampedUninvertedInternalRatio(double value) {

		double logMin = Math.log10(this.getMin());
		double logMax = Math.log10(this.getMax());
		double logRange = logMax - logMin;
		double logValue = Math.log10(value);
		double ratio = (logValue - logMin) / logRange;

		return ratio;
	}

	@Override
	public double toExternalValue(double ratio) {

		double invertedRatio = this.invert(ratio);
		double logMin = Math.log10(this.getMin());
		double logMax = Math.log10(this.getMax());
		double logRange = logMax - logMin;
		double logValue = logMin + invertedRatio * logRange;

		return Math.pow(10, logValue);
	}

}
