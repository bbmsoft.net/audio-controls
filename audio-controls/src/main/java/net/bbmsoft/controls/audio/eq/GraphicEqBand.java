package net.bbmsoft.controls.audio.eq;

import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import net.bbmsoft.controls.audio.EQ;
import net.bbmsoft.controls.audio.EQ.Band;

public class GraphicEqBand extends Control {

	private final EqBase eq;
	private final Band band;

	public GraphicEqBand(EqBase eq, EQ.Band band) {
		this.eq = eq;
		this.band = band;
	}

	public Band getBand() {
		return this.band;
	}

	public EqBase getEq() {
		return this.eq;
	}

	@Override
	protected Skin<?> createDefaultSkin() {
		return new GraphicEqBandSkin(this);
	}
}
