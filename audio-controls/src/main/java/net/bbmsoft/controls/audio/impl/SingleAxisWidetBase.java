package net.bbmsoft.controls.audio.impl;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.css.PseudoClass;
import javafx.geometry.Orientation;
import javafx.scene.control.Labeled;
import javafx.util.StringConverter;
import net.bbmsoft.controls.audio.Curve;
import net.bbmsoft.controls.audio.Scale;
import net.bbmsoft.controls.audio.SingleAxisWidget;
import net.bbmsoft.controls.audio.utils.LinearCurve;

public abstract class SingleAxisWidetBase extends TouchControlBase implements SingleAxisWidget {

	private static final PseudoClass PSEUDO_CLASS_TVERTICAL = PseudoClass.getPseudoClass("vertical");
	private static final PseudoClass PSEUDO_CLASS_THORIZONTAL = PseudoClass.getPseudoClass("horizontal");

	private final DoubleProperty value;
	private final DoubleProperty min;
	private final DoubleProperty max;
	private final BooleanProperty inverted;
	private final ObjectProperty<Orientation> orientation;
	private final ObjectProperty<Scale> scale;
	private final ObjectProperty<Labeled> label;

	private final ObjectProperty<StringConverter<Double>> tickLabelConverter;
	private final ObjectProperty<StringConverter<Double>> labelConverter;

	private final ObjectProperty<Curve> curve;

	public SingleAxisWidetBase() {

		this.value = new SimpleDoubleProperty(this, "value", 0.0) {
			@Override
			protected void invalidated() {
				double value = this.get();
				double max = getMax();
				double min = getMin();
				if (value > max || value < min) {
					this.set(clamp(min, value, max));
				}
				requestLayout();
				updateLabel(value);
			}
		};

		this.min = new SimpleDoubleProperty(this, "min", 0.0) {
			@Override
			protected void invalidated() {
				double value = valueProperty().get();
				double min = this.get();
				if (value < min) {
					valueProperty().set(min);
				}
				requestLayout();
			}
		};

		this.max = new SimpleDoubleProperty(this, "max", 1.0) {
			@Override
			protected void invalidated() {
				double value = valueProperty().get();
				double max = this.get();
				if (value > max) {
					valueProperty().set(max);
				}
				requestLayout();
			}
		};

		this.inverted = new SimpleBooleanProperty(this, "inverted", false);
		this.curve = new SimpleObjectProperty<>(this, "curve", null);
		this.orientation = new SimpleObjectProperty<>(this, "orientation", Orientation.VERTICAL);
		this.orientation.addListener((o, ov, nv) -> this.updatePseudoClass(nv));
		this.scale = new SimpleObjectProperty<Scale>(this, "scale", null);
		this.label = new SimpleObjectProperty<Labeled>(this, "label", null) {
			protected void invalidated() {
				SingleAxisWidetBase.this.updateLabel(SingleAxisWidetBase.this.getValue());
			}
		};
		this.tickLabelConverter = new SimpleObjectProperty<StringConverter<Double>>(this, "tick-label-converter", null);
		this.labelConverter = new SimpleObjectProperty<StringConverter<Double>>(this, "label-converter", null);

		this.value.addListener((o, ov, nv) -> this.updateLabel((Double) nv));
		this.scale.addListener((o, ov, nv) -> this.updateScale(ov, nv));
		this.curve.addListener((o, ov, nv) -> this.updateCurve(ov, nv));
		this.tickLabelConverter.addListener((o, ov, nv) -> this.updateTickLabelConverter(ov, nv));
		this.labelConverter.addListener((o, ov, nv) -> this.updateLabelConverter(ov, nv));

		this.setCurve(new LinearCurve());

		this.setPrefHeight(350);

		Platform.runLater(() -> this.updatePseudoClass(this.getOrientation()));
	}

	@Override
	public double getValue() {
		return this.value.get();
	}

	@Override
	public DoubleProperty valueProperty() {
		return this.value;
	}

	@Override
	public void setValue(double value) {
		this.value.set(value);
	}

	@Override
	public double getMin() {
		return this.min.get();
	}

	@Override
	public DoubleProperty minProperty() {
		return this.min;
	}

	@Override
	public void setMin(double value) {
		this.min.set(value);
	}

	@Override
	public double getMax() {
		return this.max.get();
	}

	@Override
	public DoubleProperty maxProperty() {
		return this.max;
	}

	@Override
	public void setMax(double value) {
		this.max.set(value);
	}

	@Override
	public Curve getCurve() {
		return this.curve.get();
	}

	@Override
	public void setCurve(Curve value) {
		this.curve.set(value);
	}

	@Override
	public ObjectProperty<Curve> curveProperty() {
		return this.curve;
	}

	@Override
	public Orientation getOrientation() {
		return this.orientation.get();
	}

	@Override
	public ObjectProperty<Orientation> orientationProperty() {
		return this.orientation;
	}

	@Override
	public void setOrientation(Orientation value) {
		this.orientation.set(value);
	}

	@Override
	public ObjectProperty<Scale> scaleProperty() {
		return this.scale;
	}

	@Override
	public Scale getScale() {
		return this.scale.get();
	}

	@Override
	public void setScale(Scale value) {
		this.scale.set(value);
	}

	@Override
	public ObjectProperty<Labeled> labelProperty() {
		return this.label;
	}

	@Override
	public void setLabel(Labeled label) {
		this.label.set(label);
	}

	@Override
	public Labeled getLabel() {
		return this.label.get();
	}

	@Override
	public ObjectProperty<StringConverter<Double>> tickLabelConverterProperty() {
		return this.tickLabelConverter;
	}

	@Override
	public StringConverter<Double> getTickLabelConverter() {
		return this.tickLabelConverterProperty().get();
	}

	@Override
	public void setTickLabelConverter(StringConverter<Double> value) {
		this.tickLabelConverterProperty().set(value);
	}

	protected void updateLabel(Double newValue) {

		Labeled label = this.label.get();

		if (label == null) {
			return;
		}

		StringConverter<Double> labelConverter = this.getLabelConverter();
		if (labelConverter != null) {
			label.setText(labelConverter.toString(newValue));
		}
	}

	private void updatePseudoClass(Orientation orientation) {
		this.pseudoClassStateChanged(PSEUDO_CLASS_TVERTICAL, orientation == Orientation.VERTICAL);
		this.pseudoClassStateChanged(PSEUDO_CLASS_THORIZONTAL, orientation == Orientation.HORIZONTAL);
	}

	protected void updateScale(Scale oldScale, Scale newScale) {

		if (oldScale != null) {
			oldScale.curveProperty().unbind();
			oldScale.tickLabelConverterProperty().unbind();
		}

		if (newScale != null) {
			newScale.curveProperty().bind(this.curve);
			newScale.tickLabelConverterProperty().bind(this.tickLabelConverter);
			Platform.runLater(() -> newScale.getControl().requestLayout());
		}
	}

	protected void updateCurve(Curve ov, Curve nv) {

		if (ov != null) {
			ov.minProperty().unbind();
			ov.maxProperty().unbind();
			ov.invertedProperty().unbind();
		}

		if (nv != null) {
			nv.minProperty().bind(this.min);
			nv.maxProperty().bind(this.max);
			nv.invertedProperty().bind(this.inverted);
		}

		this.requestLayout();
	}

	protected void updateTickLabelConverter(StringConverter<Double> ov, StringConverter<Double> nv) {

	}

	protected void updateLabelConverter(StringConverter<Double> ov, StringConverter<Double> nv) {

	}

	@Override
	public ObjectProperty<StringConverter<Double>> labelConverterProperty() {
		return this.labelConverter;
	}

	@Override
	public StringConverter<Double> getLabelConverter() {
		return labelConverter.get();
	}

	@Override
	public void setLabelConverter(StringConverter<Double> value) {
		this.labelConverter.set(value);
	}

	@Override
	public double getRelativeValue() {
		return this.getCurve().toInternalRatio(this.getValue());
	}

	@Override
	public double getRange() {
		return this.getMax() - this.getMin();
	}
}
