package net.bbmsoft.controls.audio.eq;

import javafx.scene.control.Skin;

public class GraphicEQ extends EqBase {

	@Override
	protected Skin<?> createDefaultSkin() {
		return new GraphicEqSkin(this);
	}
}
