package net.bbmsoft.controls.audio.utils;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import net.bbmsoft.controls.audio.Curve;

public abstract class CurveBase implements Curve {

	private final DoubleProperty min;
	private final DoubleProperty max;
	private final BooleanProperty inverted;

	public CurveBase() {
		this(0.0, 1.0, false);
	}

	public CurveBase(double min, double max, boolean inverted) {
		this.min = new SimpleDoubleProperty(this, "min", min);
		this.max = new SimpleDoubleProperty(this, "max", max);
		this.inverted = new SimpleBooleanProperty(this, "inverted", inverted);
	}

	public CurveBase(ObservableValue<Number> min, ObservableValue<Number> max, ObservableValue<Boolean> inverted) {
		this();
		this.min.bind(min);
		this.max.bind(max);
		this.inverted.bind(inverted);
	}

	@Override
	public DoubleProperty minProperty() {
		return this.min;
	}

	@Override
	public DoubleProperty maxProperty() {
		return this.max;
	}

	@Override
	public boolean isInverted() {
		return this.inverted.get();
	}

	@Override
	public BooleanProperty invertedProperty() {
		return this.inverted;
	}

	@Override
	public double toInternalRatio(double value) {

		double ratio;

		if (value <= this.getMin()) {
			ratio = 0.0;
		} else if (value >= this.getMax()) {
			ratio = 1.0;
		} else {
			ratio = this.toUnclampedUninvertedInternalRatio(value);
		}

		return this.invert(ratio);
	}

	@Override
	public double toInternalDeltaRatio(double externalFrom, double externalTo) {

		double fromRatio = this.toUnclampedUninvertedInternalRatio(externalFrom);
		double toRatio = this.toUnclampedUninvertedInternalRatio(externalTo);

		return this.isInverted() ? fromRatio - toRatio : toRatio - fromRatio;
	}

	protected abstract double toUnclampedUninvertedInternalRatio(double externalValue);

	protected double clamp(double min, double value, double max) {
		return Math.max(min, Math.min(value, max));
	}

	protected double invert(double ratio) {
		return this.isInverted() ? 1 - ratio : ratio;
	}
}
