module net.bbmsoft.audio.controls {
	
	requires transitive net.bbmsoft.audio.controls.api;
    requires transitive javafx.controls;
    requires javafx.fxml;
    requires transitive javafx.graphics;
	requires org.junit.jupiter.api;
	
	exports net.bbmsoft.controls.audio.eq;
	exports net.bbmsoft.controls.audio.fader;
	exports net.bbmsoft.controls.audio.meter;
	exports net.bbmsoft.controls.audio.scale;
	exports net.bbmsoft.controls.audio.slider;
	exports net.bbmsoft.controls.audio.utils;
	
	opens net.bbmsoft.controls.audio.impl to javafx.fxml;
}