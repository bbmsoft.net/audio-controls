package net.bbmsoft.controls.audio.fader;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import net.bbmsoft.controls.audio.scale.TouchControlScale;

public class FaderTestApp extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		TouchFader fader = new TouchFader();
		TouchControlScale scale = new TouchControlScale();
		Label label = new Label();
		TextField textField = new TextField();
		
		label.setMaxWidth(Double.MAX_VALUE);
		label.setAlignment(Pos.CENTER);
		textField.setAlignment(Pos.CENTER);

		fader.setScale(scale);
		fader.setTextInput(textField);
		fader.setLabel(label);

		double min = -66.0;
		double max = 12.0;
		
		fader.setMin(min);
		fader.setMax(max);

		for (double i = max; i >= min; i -= 3.0) {
			if(i % 6 == 0) {
				scale.getMajorTicks().add(i);
			} else {
				scale.getMinorTicks().add(i);
			}
		}

		VBox root = new VBox(label, new StackPane(scale, fader), textField);
		root.setSpacing(4);
		root.setPadding(new Insets(8));
		
		primaryStage.setScene(new Scene(root));

		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
