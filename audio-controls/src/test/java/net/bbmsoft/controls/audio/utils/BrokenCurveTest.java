package net.bbmsoft.controls.audio.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;


public class BrokenCurveTest {

	@Test
	public void testToExternalValue() {
		
		
		Map<Double, Double> linearToBroken = new HashMap<>();
		
		BrokenCurve bc = new BrokenCurve(linearToBroken);
		assertEquals(0.3, bc.toExternalValue(0.3), 0.001);
		
		linearToBroken.put(0.5, 0.3);
		bc = new BrokenCurve(linearToBroken);
		assertEquals(0.5, bc.toExternalValue(0.3), 0.001);
	}
	
	@Test
	public void testToInternalRatio() {
		
		
		Map<Double, Double> linearToBroken = new HashMap<>();
		
		BrokenCurve bc = new BrokenCurve(linearToBroken);
		assertEquals(0.3, bc.toInternalRatio(0.3), 0.001);
		
		linearToBroken.put(0.5, 0.3);
		bc = new BrokenCurve(linearToBroken);
		assertEquals(0.3, bc.toInternalRatio(0.5), 0.001);
	}
}
